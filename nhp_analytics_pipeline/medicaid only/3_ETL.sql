/* Eligibility */

truncate table eligibility_wide;
truncate table rx_claims;
truncate table medical_claims_wide;

insert into eligibility_wide
select * from nhp.eligibility_wide
where lob in ('Medicaid');

update eligibility_wide
set program_end_date = '2099-12-31'
where program_end_date = '0000-00-00';

update eligibility_wide
set eligibility_end_date = '2099-12-31'
where eligibility_end_date = '0000-00-00';

update eligibility_wide
set active_record = 0;

-- of the duplicative rows remaining, identify the current record
update eligibility_wide elig, 
	   (select max(program_end_date) as max_end_date,
                             member_uniq_id
                             from eligibility_wide
                             group by member_uniq_id) active1,
	   (select max(program_beg_date) as max_start_date,
                             member_uniq_id
                             from eligibility_wide
                             group by member_uniq_id) active2                    
set active_record = 1
where elig.member_uniq_id = active1.member_uniq_id
  and elig.program_end_date = active1.max_end_date
  and elig.member_uniq_id = active2.member_uniq_id
  and elig.program_beg_date = active2.max_start_date;

insert into medical_claims_wide
select * from nhp.medical_claims_wide
where lob in ('Medicaid')
 and date_of_service between '2015-03-01' and '2018-02-28';
 
insert into rx_claims
select * from nhp.rx_claims
where lob in ('Medicaid')
 and date_filled between '2015-03-01' and '2018-02-28';
    
