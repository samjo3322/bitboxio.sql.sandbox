-- ---------------------------------------------------------

/* set up */

-- --------------------------------------------------------

-- set this to be the same value @year_3_end in file 9_deck_profile_output
set @analysis_date = cast('2018-02-28' as date);

set @year_1_start = cast('2015-03-01' as date);
set @year_1_end = cast('2016-02-29' as date);
set @year_2_start = cast('2016-03-01' as date);
set @year_2_end = cast('2017-02-28' as date);
set @year_3_start = cast('2017-03-01' as date);
set @year_3_end = cast('2018-02-28' as date);

-- need to run this to allow for long strings in the group_concat descriptions below
set group_concat_max_len = 80000000;

-- create a seperate table used to identify savings start dates, based on the table of qualified members used in profling.   Note this is a distinct listing.
drop table if exists savings_claims;
CREATE TABLE `savings_claims` (
  `member_uniq_id` varchar(55) DEFAULT NULL,
  `lob` varchar(55) DEFAULT NULL,
  `claim_number` varchar(55) DEFAULT NULL,
  `claim_line_number` int(11) DEFAULT NULL,
  `date_of_service` date DEFAULT NULL,
  `service_start_date` date DEFAULT NULL,
  `service_end_date` date DEFAULT NULL,
  `amt_allowed` decimal(14,2) DEFAULT NULL,
  `amt_paid` decimal(14,2) DEFAULT NULL,
  `cms_place_of_service` varchar(5) DEFAULT NULL,
  `service_units` int(11) DEFAULT NULL,
  `claim_status` varchar(55) DEFAULT NULL,
  `icd_1` varchar(55) DEFAULT NULL,
  `icd_2` varchar(55) DEFAULT NULL,
  `icd_3` varchar(55) DEFAULT NULL,
  `icd_4` varchar(55) DEFAULT NULL,
  `icd_5` varchar(55) DEFAULT NULL,
  `icd_6` varchar(55) DEFAULT NULL,
  `icd_7` varchar(55) DEFAULT NULL,
  `icd_8` varchar(55) DEFAULT NULL,
  `icd_9` varchar(55) DEFAULT NULL,
  `icd_10` varchar(55) DEFAULT NULL,
  `rolling_year` int(11) DEFAULT NULL,
  `exclude_from_calcs` int(11) DEFAULT NULL,
  `fully_adjudicated` int(11) DEFAULT NULL,
  KEY `claim` (`claim_number`(12)),
  KEY `member_uniq_id` (`member_uniq_id`(12)),
  KEY `lob` (`lob`(8))
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert into savings_claims
select distinct   
  `member_uniq_id`,
  `lob`,
  `claim_number`,
  `claim_line_number`,
  `date_of_service`,
  `service_start_date`,
  `service_end_date`,
  `amt_allowed`,
  `amt_paid`,
  `cms_place_of_service`,
  `service_units`,
  `claim_status`,
  `icd_1`,
  `icd_2`,
  `icd_3`,
  `icd_4`,
  `icd_5`,
  `icd_6`,
  `icd_7`,
  `icd_8`,
  `icd_9`,
  `icd_10`,
  `rolling_year`,
  `exclude_from_calcs`,
  `fully_adjudicated`
from report_claims;

/* end set up */

-- ----------------------------------------------------

/*

Identify the date of the earliest primary diangosis for each member

-- ---------------------------------------------------

/* 

find the qualifying DX date for each member with a primary condition 

Note that the "or" prhasing in the join can create many to one results can become problematic if you use this concept to answer other questions.

*/

drop table if exists qualified_primary_diagnosis_claims;
create table qualified_primary_diagnosis_claims (
member_uniq_id varchar(55),
claim_number varchar(255),
claim_line_number int,
date_of_service date,
service_start_date date,
service_end_date date,
icd_code_from_claim varchar(11),
code_description varchar(255),
MonthLag int,
lag_type varchar(5),
months_count int,
key clm_number (claim_number(55) ASC),
key member (member_uniq_id(11) ASC)
);

truncate table qualified_primary_diagnosis_claims;
insert into qualified_primary_diagnosis_claims
select distinct a.member_uniq_id,
       a.claim_number,
       null as claim_line_number,
	   a.date_of_service,
       null as service_start_date,
	   null as service_end_date,
       a.icd_code_from_claim,
       a.code_description,
       timestampdiff(month,date_of_service,cast(@analysis_date as date)) as MonthLag,
       b.lag_type,
       b.months_count
from  member_dx_details_long a
  join resources.uh_dx_icd_final b
    on (a.icd_code_from_claim = b.icd_10
      or a.icd_code_from_claim = b.icd_9)
  and lag_type = '<'
  and timestampdiff(month,date_of_service,cast(@analysis_date as date)) < months_count
  and date_of_service <= cast(@analysis_date as date)
  and date_of_service between @year_1_start and @year_3_end
union
select distinct a.member_uniq_id,
       a.claim_number,
       null as claim_line_number,
	   a.date_of_service,
       null as service_start_date,
	   null as service_end_date,
       a.icd_code_from_claim,
       a.code_description,
       timestampdiff(month,date_of_service,cast(@analysis_date as date)) as MonthLag,
       b.lag_type,
       b.months_count
from member_dx_details_long a 
  join resources.uh_dx_icd_final b
    on (a.icd_code_from_claim = b.icd_10
      or a.icd_code_from_claim = b.icd_9)
where lag_type = '>'
  and timestampdiff(month,date_of_service,cast(@analysis_date as date)) > months_count
  and date_of_service <= cast(@analysis_date as date) 
  and date_of_service between @year_1_start and @year_3_end
union
select distinct a.member_uniq_id,
       a.claim_number,
       null as claim_line_number,
	   a.date_of_service,
       null as service_start_date,
	   null as service_end_date,
       a.icd_code_from_claim,
       a.code_description,
       timestampdiff(month,date_of_service,cast(@analysis_date as date)) as MonthLag,
       b.lag_type,
       b.months_count
from member_dx_details_long a
  join resources.uh_dx_icd_final b
    on (a.icd_code_from_claim = b.icd_10
      or a.icd_code_from_claim = b.icd_9)
where lag_type is null
  and b.is_primary = 1
  and date_of_service <= cast(@analysis_date as date)
  and date_of_service between @year_1_start and @year_3_end
;


/* Now need to find the earliest qualifying DX for each member and store that date to pull all secondary claims */

-- Pull earliest qualified date for members who are Eligible at the time of the report

drop table if exists member_primary_dx_start_date;
create table member_primary_dx_start_date
(
member_uniq_id varchar(55),
min_date_of_service date,
lag_type_list varchar(55),
claims_count int,
primary_icd_code_count int,
claim_number_list TEXT,
primary_icd_code_list TEXT,
eligibility_status varchar(1),
Key member (member_uniq_id (11))
);

insert into member_primary_dx_start_date
select a.member_uniq_id,
       min(date_of_service),
       group_concat(distinct lag_type),
       count(distinct claim_number),
       count(distinct icd_code_from_claim),
       group_concat(distinct claim_number order by date_of_service ASC separator '|'),
	   group_concat(distinct icd_code_from_claim order by date_of_service ASC separator '|'),
       max(eligibility_status)
from qualified_primary_diagnosis_claims a
 left join eligibility_wide b
    on a.member_uniq_id = b.member_uniq_id
group by a.member_uniq_id;

ALTER TABLE member_primary_dx_start_date 
add column earliest_claim varchar(55),
add column earliest_ICD varchar(11);

update member_primary_dx_start_date
set earliest_claim = case when locate('|',claim_number_list) > 0 then substring(claim_number_list,1,locate('|',claim_number_list) - 1 )
                          else claim_number_list end,
    earliest_ICD = case when locate('|',primary_icd_code_list) > 0 then substring(primary_icd_code_list,1,locate('|',primary_icd_code_list) - 1 )
                          else primary_icd_code_list end;

-- -------------------------------------------------------------

/* 

Identify claims tied to IOC that should be excluded per clinical expertise as identified in the uh_dx_icd_final table

*/

-- pull IOC definition from claims
drop table if exists acute_IOC_to_exclude;
create table acute_IOC_to_exclude 
(
member_uniq_id varchar(55),
service_start_date date,
service_end_date date
);

-- identify IOC with an acute DX
insert into acute_IOC_to_exclude
select distinct member_uniq_id,
                service_start_date,
                service_end_date
from medical_claims_wide
where claim_number in (select distinct claim_number
                       from member_dx_details_long a
                         join resources.uh_dx_icd_final b
                           on a.icd_code_from_claim = b.icd_10
                             or a.icd_code_from_claim = b.icd_9
                       where exclude_acute_presentation = 1
);

-- identify all claims tied to the acute DX
drop table if exists acute_claims_to_exclude;
create table acute_claims_to_exclude 
(
member_uniq_id varchar(55),
claim_number varchar(255),
claim_line_number int,
amt_allowed decimal(14,2),
amt_paid decimal(14,2),
claim_status varchar(1),
procedure_code varchar(8),
rev_code varchar(4),
drg varchar(4),
date_of_service date,
service_start_date date,
service_end_date date,
icd_1 varchar(8),
icd_2 varchar(8),
icd_3 varchar(8),
icd_4 varchar(8),
icd_5 varchar(8),
icd_6 varchar(8),
icd_7 varchar(8),
icd_8 varchar(8),
icd_9 varchar(8),
icd_10 varchar(8),
 KEY mem_id (member_uniq_id(10)),
 Key claim_number (claim_number(10)),
 Key procedure_code (procedure_code(8))
);

truncate acute_claims_to_exclude;
insert into acute_claims_to_exclude
select a.member_uniq_id,
       a.claim_number,
       a.claim_line_number,
       a.amt_allowed,
       a.amt_paid,
       a.claim_status,
       a.procedure_code,
       a.rev_code,
       a.drg,
       a.date_of_service,
       a.service_start_date,
       a.service_end_date,
       a.icd_1,
       a.icd_2,
       a.icd_3,
       a.icd_4,
       a.icd_5,
       a.icd_6,
       a.icd_7,
       a.icd_8,
       a.icd_9,
	   a.icd_10
from medical_claims_wide a, 
     acute_IOC_to_exclude b
where a.member_uniq_id = b.member_uniq_id
  and a.service_start_date = b.service_start_date
  and a.service_end_date = b.service_end_date;

-- -------------------------------------------------------------

/*

Pull in all claims for member after their qualifying primary dx to apply the savings factor 
  and populate the exlcusion criteria for acute and members

*/

-- --------------------------------------------------------------

drop table if exists presales_claims_and_savings;
create table presales_claims_and_savings
(
 member_uniq_id varchar(255) DEFAULT NULL,
 icd_code_from_claim varchar(20) DEFAULT NULL,
  claim_number varchar(255) DEFAULT NULL,
  date_of_service date DEFAULT NULL,
  icd_10 varchar(20) DEFAULT NULL,
  icd_9 varchar(20) DEFAULT NULL,
  descriptive_columns_version int(11) DEFAULT NULL,
  category varchar(50) DEFAULT NULL,
  code_description text,
  savings_category text,
  is_primary int(1) DEFAULT NULL,
  is_ACSC int(1) DEFAULT '0',
  is_secondary int(1) DEFAULT NULL,
  chronicity varchar(50) DEFAULT NULL,
  active_start_date date DEFAULT NULL,
  active_end_date date DEFAULT NULL,
  active_flag int(11) DEFAULT NULL,
  months_count int(11) DEFAULT NULL,
  lag_type varchar(55) DEFAULT NULL,
  time_frame_description varchar(255) DEFAULT NULL,
  savings_factor decimal(14,2) DEFAULT NULL,
  exclude_acute_presentation int(11) DEFAULT NULL,
  percent_savings_raw varchar(45) DEFAULT NULL,
  eligibility_status varchar(1) default Null,
  icd_join int default null,
  KEY catagory (category),
  KEY icd_10 (icd_10),
  KEY icd_9 (icd_9),
  KEY Isacsc (is_ACSC),
  KEY member_uniq_id (member_uniq_id),
  KEY icd (icd_code_from_claim),
  KEY claim_number (claim_number),
  KEY date (date_of_service),
  KEY isPrimary (is_Primary),
  KEY isSecondary (is_Secondary)
) 
ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into presales_claims_and_savings
select  
  a.member_uniq_id,
  a.icd_code_from_claim,
  a.claim_number,
  a.date_of_service,
  b.icd_10,
  b.icd_9,
  b.descriptive_columns_version,
  b.category,
  b.code_description,
  b.savings_category,
  b.is_primary,
  b.is_ACSC,
  b.is_secondary,
  b.chronicity,
  b.active_start_date,
  b.active_end_date,
  b.active_flag,
  b.months_count,
  b.lag_type,
  b.time_frame_description,
  b.savings_factor,
  b.exclude_acute_presentation,
  b.percent_savings_raw,
  c.eligibility_status,
  9 as icd_join
from member_dx_details_long a
  join resources.uh_dx_icd_final b
    on a.icd_code_from_claim = b.icd_9
  join member_primary_dx_start_date c 
    on a.member_uniq_id = c.member_uniq_id
where a.date_of_service > c.min_date_of_service
and date_of_service between @year_1_start and @year_3_end;

insert into presales_claims_and_savings
select 
  a.member_uniq_id,
  a.icd_code_from_claim,
  a.claim_number,
  a.date_of_service,
  b.icd_10,
  b.icd_9,
  b.descriptive_columns_version,
  b.category,
  b.code_description,
  b.savings_category,
  b.is_primary,
  b.is_ACSC,
  b.is_secondary,
  b.chronicity,
  b.active_start_date,
  b.active_end_date,
  b.active_flag,
  b.months_count,
  b.lag_type,
  b.time_frame_description,
  b.savings_factor,
  b.exclude_acute_presentation,
  b.percent_savings_raw,
  c.eligibility_status,
  10 as icd_join
from member_dx_details_long a
  join resources.uh_dx_icd_final b
    on a.icd_code_from_claim = b.icd_10
  join member_primary_dx_start_date c
    on a.member_uniq_id = c.member_uniq_id
where a.date_of_service > c.min_date_of_service
  and date_of_service between @year_1_start and @year_3_end 
;
							
alter table presales_claims_and_savings
add column acute_exclusion int default 0;

alter table presales_claims_and_savings
add column member_exclusion int default 1;

update presales_claims_and_savings a,
       acute_claims_to_exclude b
set a.acute_exclusion = 1
where a.claim_number = b.claim_number;

/* 
this is somewhat unclear: due to operations and table stucture, we need to reference the report_claims table to limit members in
   the savings calcluations to only those in our qualified pool in our saving table below.  By applying the member_exclusion filter
   and setting it to 0 for members that should be included in the savings analysis, we can ensure that our cohorts are consistant alter
*/

update presales_claims_and_savings a,
       report_claims b
set a.member_exclusion = 0
where a.claim_number = b.claim_number;

-- ---------------------------------------------------

/*

Collapse claim lines to the claim level and join savings factor level data

*/

-- ---------------------------------------------------

drop table if exists summed_savings_claims;
create table summed_savings_claims 
(
claim_number varchar(55),
amt_allowed decimal(14,2),
amt_paid decimal(14,2),
member_uniq_id varchar(55),
lob varchar(25),
rolling_year varchar(4),
Key claim_number (claim_number(11)),
Key member_uniq_id (member_uniq_id(11))

);

insert into summed_savings_claims
select claim_number as claim_number,
	   sum(amt_allowed) as amt_allowed,
       sum(amt_paid) as amt_paid,
       member_uniq_id,
       lob,
       case when a.date_of_service between '2017-07-01' and '2018-06-30' then 2018
            when a.date_of_service between '2016-07-01' and '2017-06-30' then 2017
            when a.date_of_service between '2015-07-01' and '2016-06-30' then 2016 end as rolling_year
from medical_claims_wide a
where claim_number in (select claim_number from presales_claims_and_savings)
group by claim_number,
         member_uniq_id,
         lob,
         case when a.date_of_service between '2017-07-01' and '2018-06-30' then 2018
              when a.date_of_service between '2016-07-01' and '2017-06-30' then 2017
              when a.date_of_service between '2015-07-01' and '2016-06-30' then 2016 end;
	
drop table if exists claims_savings_factor;
create table claims_savings_factor 
(
claim_number varchar(55),
savings_factor decimal(14,2),
savings_cat_count int,
savings_cat_list varchar(8000),
max_savings_cat varchar(255),
member_uniq_id varchar(55),
eligibility_status varchar(1),
acute_exclusion int,
Key claim_number (claim_number(11)),
Key member_uniq_id (member_uniq_id(11))
);  

-- find the highgest level of savings available for each claim, based on the available savings factors

insert into claims_savings_factor
select claim_number, 
	   max(savings_factor) as savings_factor,
	   count(distinct savings_category),
       group_concat(distinct savings_category order by savings_factor DESC separator '|'),
       null,
       member_uniq_id,
       eligibility_status,
       acute_exclusion
from presales_claims_and_savings
  where member_exclusion = 0   -- this flag should ALWAYS be set to 0
group by claim_number, member_uniq_id, acute_exclusion;

update claims_savings_factor
set max_savings_cat =  case when locate('|',savings_cat_list) > 0 then substring(savings_cat_list,1,locate('|',savings_cat_list) - 1 )
	else savings_cat_list end;

drop table if exists savings_by_claim_number;
create table savings_by_claim_number
(
claim_number varchar(55),
amt_allowed decimal(14,2),
amt_paid decimal(14,2),
member_uniq_id varchar(55),
lob varchar(55),
condition_count int,
savings_factor decimal(14,2),
savings_cat_list varchar(8000),
max_savings_cat varchar(255),
rolling_year varchar(4),
exposure_saved decimaL(14,2),
spend_saved decimal(14,2),
acute_exclusion int,
key claim_number (claim_number(11)),
key member_uniq_id (member_uniq_id(11))
);

insert into savings_by_claim_number
select a.claim_number,
       a.amt_allowed,
       a.amt_paid,
       a.member_uniq_id,
       a.lob,
       c.condition_count,
       b.savings_factor,
       b.savings_cat_list,
       b.max_savings_cat,
       a.rolling_year,
	   a.amt_allowed * b.savings_factor as exposure_saved,
       a.amt_paid * b.savings_factor as spend_saved,
       b.acute_exclusion
from summed_savings_claims a
  join claims_savings_factor b
    on a.claim_number = b.claim_number
  join comingling_members_conditions c
    on a.member_uniq_id = c.member;

-- -------------------------------------------------------
/*
create output tables
*/
-- -------------------------------------------------------
drop table if exists savings_by_condition_count;
create table savings_by_condition_count
(
lob varchar(55),
rolling_year varchar(4),
condition_count int,
totalSpend decimal(14,2),
totalSaved decimal(14,2),
memberCount int,
acute_exclusion_applied varchar(1)
);

-- savings by condition count and LOB all years
insert into savings_by_condition_count
select lob,
       'All',
	   condition_count, 
	   sum(amt_paid),
	   sum(spend_Saved),
       count(distinct member_uniq_id),
       'N'
from savings_by_claim_number
 where rolling_year is not null
group by lob, condition_count, 'All';

insert into savings_by_condition_count
select lob,
       'All',
	   condition_count, 
	   sum(amt_paid),
	   sum(spend_Saved),
       count(distinct member_uniq_id),
       'Y'
from savings_by_claim_number
 where rolling_year is not null
   and acute_exclusion = 1
group by lob, condition_count, 'All';

-- savings by condition count, LOB and year
insert into savings_by_condition_count
select lob,
       rolling_year,
	   condition_count, 
	   sum(amt_paid),
	   sum(spend_Saved),
       count(distinct member_uniq_id),
       'N'
from savings_by_claim_number
where rolling_year is not null
group by lob, condition_count, rolling_year;

insert into savings_by_condition_count
select lob,
       rolling_year,
	   condition_count, 
	   sum(amt_paid),
	   sum(spend_Saved),
       count(distinct member_uniq_id),
       'Y'
from savings_by_claim_number
where rolling_year is not null
   and acute_exclusion = 1
group by lob, condition_count, rolling_year;

drop table if exists savings_by_condition;
create table savings_by_condition
(
lob varchar(55),
rolling_year varchar(4),
savingsCat varchar(255),
savingsCatList varchar(8000),
totalSpend decimal(14,2),
totalSaved decimal(14,2),
memberCount int,
acute_exclusion_applied varchar(1)
);

-- pull savings numbers by condition
insert into savings_by_condition
select lob,
       rolling_year,
       max_savings_cat,
       max(savings_cat_list),
	   sum(amt_paid),
       sum(spend_Saved), 
       count(distinct member_uniq_id),
       'N'
from savings_by_claim_number
where rolling_year is not null
group by lob, rolling_year, max_savings_cat
order by sum(spend_Saved) desc;

insert into savings_by_condition
select lob,
       rolling_year,
       max_savings_cat,
       max(savings_cat_list),
	   sum(amt_paid),
       sum(spend_Saved), 
       count(distinct member_uniq_id),
       'Y'
from savings_by_claim_number
where rolling_year is not null
   and acute_exclusion = 1
group by lob, rolling_year, max_savings_cat
order by sum(spend_Saved) desc;

-- pull numbers by POS
drop table if exists claim_pos; 
create table claim_pos
(
claim_number varchar(55),
pos varchar(2),
key claim_number (claim_number(12)),
key pos (pos(2))
);

drop table if exists savings_by_pos;
create table savings_by_pos
(
lob varchar(55),
rolling_year varchar(4),
placeOfService varchar(255),
totalSpend decimal(14,2),
totalSaved decimal(14,2),
memberCount int,
acute_exclusion_applied varchar(1)
);

insert into claim_pos
select claim_number, 
       max(cms_place_of_service_id)
from medical_claims_wide
where claim_number in (select claim_number from presales_claims_and_savings)
  and date_of_service between @year_1_start and @year_3_end
group by claim_number;

insert into savings_by_pos
select lob,
       rolling_year,
       c.name,
	   sum(amt_paid),
       sum(spend_Saved), 
       count(distinct member_uniq_id),
       'N'
from savings_by_claim_number a
  join claim_pos b
    on a.claim_number = b.claim_number
 left join resources.pos c
    on ifnull(b.pos,'') = c.pos
where rolling_year is not null
group by lob, rolling_year, c.name
order by sum(spend_Saved) desc;

insert into savings_by_pos
select lob,
       rolling_year,
       c.name,
	   sum(amt_paid),
       sum(spend_Saved), 
       count(distinct member_uniq_id),
       'Y'
from savings_by_claim_number a
  join claim_pos b
    on a.claim_number = b.claim_number
 left join resources.pos c
    on ifnull(b.pos,'') = c.pos
where rolling_year is not null
   and acute_exclusion = 1
group by lob, rolling_year, c.name
order by sum(spend_Saved) desc;


-- ------------------------------------------------
/*

generate outoupt

*/
-- --------------------------------------------------

select * from savings_by_condition_count
where acute_exclusion_applied = 'N'
order by lob, rolling_year;

select * from savings_by_condition_count
where acute_exclusion_applied = 'Y'
order by lob, rolling_year;

select * from savings_by_condition
where acute_exclusion_applied = 'N'
order by lob, rolling_year;

select * from savings_by_condition
where acute_exclusion_applied = 'Y'
order by lob, rolling_year;

select * from savings_by_pos
where acute_exclusion_applied = 'N'
order by lob, rolling_year;

select * from savings_by_pos
where acute_exclusion_applied = 'Y'
order by lob, rolling_year;

