-- --------------------------------------------------------------------
      
/* 

Output Results

to may need to join/ modify these tables (or the code used to create them) to meet custom client follow up requests

*/

-- --------------------------------------------------------------------
set @year_1_start = cast('2015-03-01' as date);
set @year_1_end = cast('2016-02-29' as date);
set @year_2_start = cast('2016-03-01' as date);
set @year_2_end = cast('2017-02-28' as date);
set @year_3_start = cast('2017-03-01' as date);
set @year_3_end = cast('2018-02-28' as date);

/* raw data report */
select md.*,
       rx.claimsCount as RXClaimsCount,
       rx.memberCount as RXMemberCount,
       rx.totalSpend as RXSpend
from
(
 select lob,
        case when date_of_service between @year_1_start and @year_1_end then 2016
			 when date_of_service between @year_2_start and @year_2_end then 2017
	 		 when date_of_service between @year_3_start and @year_3_end then 2018 end as rolling_year,
        sum(amt_paid) as totalSpend,
        count(distinct claim_number) as ClaimsCount,
        count(distinct member_uniq_id) as MemberCount,
        max(date_of_service) as startDate,
        min(date_of_service) as endDate
 from medical_claims_wide
 where date_of_service between @year_1_start and @year_3_end
 group by lob, 
          case when date_of_service between @year_1_start and @year_1_end then 2016
			   when date_of_service between @year_2_start and @year_2_end then 2017
	 		   when date_of_service between @year_3_start and @year_3_end then 2018 end) md
join
(
 select lob,
        case when date_filled between @year_1_start and @year_1_end then 2016
			 when date_filled between @year_2_start and @year_2_end then 2017
	 		 when date_filled between @year_3_start and @year_3_end then 2018 end as rolling_year,
        sum(amt_paid) as totalSpend,
        count(distinct claim_number) as ClaimsCount,
        count(distinct member_uniq_id) as MemberCount
 from rx_claims
 where date_filled between @year_1_start and @year_3_end
 group by lob, 
          case when date_filled between @year_1_start and @year_1_end then 2016
			   when date_filled between @year_2_start and @year_2_end then 2017
	 		   when date_filled between @year_3_start and @year_3_end then 2018 end) rx
on md.lob = rx.lob
  and md.rolling_year = rx.rolling_year;
 
       

 -- use for overall metrics by category (Filtered Members Overall)
select * from overall_metrics;

-- yearly member spend (Member Spend Breakdown)
select * from year_member_spend_all;


 -- use to populate category level line items (Filtered Members)
select * from metrics_by_category;

 -- use to populate comingling members by condition count (Condition Comingling Overall, Condition Comingling Rolling Yearly by LOB)
select   rolling_year, lob, category_count, totalPaid as medicalSpend, memberCount as medicalMemberCount
from comingling_metrics 
order by lob, rolling_year, category_count;

-- use to populate comingling members by condition count broken out by condition descriptions (Condition Comingling Overall, Condition Comingling Rolling Yearly by LOB)
select rolling_year, lob, conditions, condition_count, memberCount, totalSpend 
from comingling_members_conditions_metrics;

-- use for more cominlinging reporting (Condition Comingling Rolling Yearly)
select rolling_year, lob, condition_count, sum(memberCount), sum(totalSpend)
from comingling_members_conditions_metrics
where lob = 'All' -- use this filter as needed
group by rolling_year, lob, condition_count;

-- use these to populate the RX values in sheets (Most previous sheets)
select memberCount, totalSpend, rolling_year,lob 
from rx_metrics 
where cohort = 'Qualified Members'
order by lob, rolling_year;

select lob, rolling_year, category, TotalPaid, MemberCount
from rx_condition_metrics 
order by lob, rolling_year, category;

-- use for populating single member metric sheet
select * from single_condition_member_metrics_by_category;
select * from single_condition_rx_by_condition order by lob, rolling_year, category;

-- use to populate pos metrics sheet
select lob, rolling_year, category_desc_or_count, pos_description,  sum(totalSpend), sum(memberCount)
from pos_utilization_metrics
where cohort like 'Qualified%'
group by lob, pos_description,category_desc_or_count, rolling_year, cohort
order by rolling_year, pos_description;

select lob, rolling_year, category_desc_or_count, pos_description,  sum(totalSpend), sum(memberCount)
from pos_utilization_metrics
where cohort like 'Qualified%'
  and admit_ind = 'Y'
group by lob, pos_description,category_desc_or_count, rolling_year, cohort
order by rolling_year, pos_description;

-- Output from Step 8 (member profling)  ------------------------------

-- by bucket and category: all members
select  bucket_category,
        lob,
        count(distinct claim_number), 
        count(distinct member_uniq_id), 
        sum(amt_paid)
from member_presales_claims_profile a
where date_of_service between @year_1_start and @year_3_end
group by bucket_category, lob;

-- by bucket and category: comingling members
select  bucket_category,
        Category_count,
		lob,
        count(distinct claim_number), 
        count(distinct member_uniq_id), 
        sum(amt_paid)
from member_presales_claims_profile a
  join comingling_members b
    on a.member_uniq_id = b.Member
where date_of_service between @year_1_start and @year_3_end
group by bucket_category, category_count, lob;

-- by bucket, year and category: All members
select bucket_category,
       lob,
	   case when a.date_of_service between @year_1_start and @year_1_end then 2016
            when a.date_of_service between @year_2_start and @year_2_end then 2017
            when a.date_of_service between @year_3_start and @year_3_end then 2018 end as rolling_year,
        count(distinct claim_number), 
        count(distinct member_uniq_id), 
        sum(amt_paid)
from member_presales_claims_profile a
where date_of_service between @year_1_start and @year_3_end
group by bucket_category,
         lob,
	     case when a.date_of_service between @year_1_start and @year_1_end then 2016
              when a.date_of_service between @year_2_start and @year_2_end then 2017
              when a.date_of_service between @year_3_start and @year_3_end then 2018 end;

-- by bucket, year and category: comingling members
select bucket_category,
       Category_count,
	   lob,
	   case when a.date_of_service between @year_1_start and @year_1_end then 2016
            when a.date_of_service between @year_2_start and @year_2_end then 2017
            when a.date_of_service between @year_3_start and @year_3_end then 2018 end as rolling_year,
        count(distinct claim_number), 
        count(distinct member_uniq_id), 
        sum(amt_paid)
from member_presales_claims_profile a
  join comingling_members b
    on a.member_uniq_id = b.Member
  where  date_of_service between @year_1_start and @year_3_end
group by bucket_category,  
         Category_count,     
         lob,
	   case when a.date_of_service between @year_1_start and @year_1_end then 2016
            when a.date_of_service between @year_2_start and @year_2_end then 2017
            when a.date_of_service between @year_3_start and @year_3_end then 2018 end;