/* Create date variables */

/*
 profile dataset to see what cutoffs make sense

select count(*), count(distinct claim_number),
       cast(concat(year(date_of_service),'-',month(date_of_service),'-01') as date)
from medical_claims_wide
group by cast(concat(year(date_of_service),'-',month(date_of_service),'-01') as date);
*/


-- assign variables
set @year_1_start = cast('2015-03-01' as date);
set @year_1_end = cast('2016-02-29' as date);
set @year_2_start = cast('2016-03-01' as date);
set @year_2_end = cast('2017-02-28' as date);
set @year_3_start = cast('2017-03-01' as date);
set @year_3_end = cast('2018-02-28' as date);

/* 

Deactivated on 12/11/18 because we got LOB information and updated the LOB
-- since no clear LOB during ETL, set here for reporting
update medical_claims_wide
set lob = 'All NHP';

update rx_claims
set lob = 'All NHP';

*/
-- -------------------------------------------------------------------------------

/* 

Seed basic claims data

*/

-- -----------------------------------------------------------------------------------

drop table if exists report_claims;
create table report_claims
(
member_uniq_id varchar(55),
lob varchar(55),
claim_number varchar(55),
claim_line_number int,
date_of_service date,
service_start_date date,
service_end_date date,
amt_allowed decimal(14,2),
amt_paid decimal(14,2),
cms_place_of_service varchar(5),
service_units int,
claim_status varchar(55),
icd_1 varchar(55),
icd_2 varchar(55),
icd_3 varchar(55),
icd_4 varchar(55), 
icd_5 varchar(55),
icd_6 varchar(55),
icd_7 varchar(55),
icd_8 varchar(55),
icd_9 varchar(55),
icd_10 varchar(55),
category varchar(55),
rolling_year int,
exclude_from_calcs int,
fully_adjudicated int,
key claim (claim_number(12)),
key member_uniq_id (member_uniq_id(12)),
key lob (lob(8)),
key category (category(8))
); 

drop table if exists member_claims_for_report;
create table member_claims_for_report
(
member_uniq_id varchar(55),
category varchar(55),
Key member (member_uniq_id (11)),
key category (category(12))
);

truncate member_claims_for_report;
insert into member_claims_for_report
select distinct a.member_uniq_id,
			    case when category = 'Hemorrhage' then 'Stroke'
                     when category = 'Infarction' then 'Stroke'
                     when category = 'Infaction' then 'Stroke'
					 when category = 'Secondary' then 'Other CV Dz' 
	                 else category end as category
from member_dx_details_long a
where isPrimary = 1
  and date_of_service between @year_1_start and @year_3_end
  and member_uniq_id in (select distinct member_uniq_id from members_spend_filter)
  and category not in ('Burns','comps of care','ContusionsAbrasions','Fractures','Neoplasm','Osteoporosis','SprainsDislocSoftTissueInj')
  and icd_code_from_claim not in (select icd_code from resources.injury_codes_to_remove);

truncate report_claims;
insert into report_claims
select a.member_uniq_id,
       lob,
       a.claim_number,
       a.claim_line_number,
	   a.date_of_service,
       a.service_start_date,
       a.service_end_date,
       a.amt_allowed,
       a.amt_paid,
       cms_place_of_service_id,
       service_units,
       claim_status,
       icd_1,
       icd_2,
       icd_3,
       icd_4,
       icd_5,
       icd_6,
       icd_7,
       icd_8,
       icd_9,
       icd_10,
       category,
	   case when a.date_of_service between @year_1_start and @year_1_end then 2016
            when a.date_of_service between @year_2_start and @year_2_end then 2017
            when a.date_of_service between @year_3_start and @year_3_end then 2018 end as rolling_year,
       exclude_from_calcs,
       fully_adjudicated
from medical_claims_wide a
 join member_claims_for_report b
   on a.member_uniq_id = b.member_uniq_id;
   
-- -------------------------------------------------------

/* Pull overall metrics 

Needs seperate section to prevent double counting across categories

*/

-- --------------------------------------------------------
drop table if exists overall_metrics;
create table overall_metrics
(
claim_number varchar(22),
amt_paid decimal(14,2),
amt_allowed decimal(14,2),
member_count int,
year varchar(4),
lob varchar(22)
);


--  All LOB All Years
truncate table overall_metrics;
insert into overall_metrics
select count(distinct claim_number),
       sum(a.amt_paid),
       sum(a.amt_allowed),
       count(distinct a.member_uniq_id),
       'All',
       'All'
from medical_claims_wide a
where date_of_service between @year_1_start and @year_3_end
  and member_uniq_id in (select distinct member_uniq_id from members_spend_filter)
  and a.member_uniq_id in (select d.member_uniq_id 
                           from member_dx_details_long d 
                           where isPrimary = 1 
                             and date_of_service between @year_1_start and @year_3_end
							 and category not in ('Burns','comps of care','ContusionsAbrasions','Fractures','Neoplasm','Osteoporosis','SprainsDislocSoftTissueInj')
                             and icd_code_from_claim not in (select icd_code from resources.injury_codes_to_remove))
group by 'All','All';

-- All Years by LOB
insert into overall_metrics
select count(distinct claim_number),
       sum(a.amt_paid),
       sum(a.amt_allowed),
       count(distinct a.member_uniq_id),
       'All',
       lob
from medical_claims_wide a
where date_of_service between @year_1_start and @year_3_end
  and member_uniq_id in (select distinct member_uniq_id from members_spend_filter)
  and a.member_uniq_id in (select d.member_uniq_id 
                           from member_dx_details_long d 
                           where isPrimary = 1 
                             and date_of_service between @year_1_start and @year_3_end
							 and category not in ('Burns','comps of care','ContusionsAbrasions','Fractures','Neoplasm','Osteoporosis','SprainsDislocSoftTissueInj')
                             and icd_code_from_claim not in (select icd_code from resources.injury_codes_to_remove))
group by 'All',lob;

-- By year and by LOB 
insert into overall_metrics
select count(distinct claim_number),
       sum(a.amt_paid),
       sum(a.amt_allowed),
       count(distinct a.member_uniq_id),
	   case when a.date_of_service between @year_1_start and @year_1_end then 2016
            when a.date_of_service between @year_2_start and @year_2_end then 2017
            when a.date_of_service between @year_3_start and @year_3_end then 2018 end as rolling_year,
       lob
from medical_claims_wide a
where date_of_service between @year_1_start and @year_3_end
  and member_uniq_id in (select distinct member_uniq_id from members_spend_filter)
  and a.member_uniq_id in (select d.member_uniq_id 
                           from member_dx_details_long d 
                           where isPrimary = 1 
                             and date_of_service between @year_1_start and @year_3_end
							 and category not in ('Burns','comps of care','ContusionsAbrasions','Fractures','Neoplasm','Osteoporosis','SprainsDislocSoftTissueInj')
                             and icd_code_from_claim not in (select icd_code from resources.injury_codes_to_remove))
group by lob, 
	   case when a.date_of_service between @year_1_start and @year_1_end then 2016
            when a.date_of_service between @year_2_start and @year_2_end then 2017
            when a.date_of_service between @year_3_start and @year_3_end then 2018 end;
              
-- -----------------------------------

/* 

Pull numbers by category

*/

-- -----------------------------------

drop table if exists metrics_by_category;
create table metrics_by_category
(
category varchar(55),
memberCount int,
totalPaid decimal(14,2),
lob varchar(55),
rolling_year varchar(4),
key category (category(12)),
key lob (lob(10))
);

insert into metrics_by_category
select category,
       count(distinct member_uniq_id) as MemberCount,
	   sum(amt_paid) as TotalPaid,
       'All',
       'All'
from report_claims
where date_of_service between @year_1_start and @year_3_end
group by category
order by category;

insert into metrics_by_category
select category,
       count(distinct member_uniq_id) as MemberCount,
	   sum(amt_paid) as TotalPaid,
       lob,
	   'All'
from report_claims
where date_of_service between @year_1_start and @year_3_end
group by lob, 
		 category
order by lob, category;

insert into metrics_by_category
select category,
       count(distinct member_uniq_id) as MemberCount,
	   sum(amt_paid) as TotalPaid,
       lob,
	   rolling_year
from report_claims
where date_of_service between @year_1_start and @year_3_end
group by lob, 
		 category,
         rolling_year
order by lob, rolling_year, category;


-- -----------------------------------------------------

/* Comingling */

-- -----------------------------------------------------

drop table if exists comingling_members;
create table comingling_members
(
member varchar(22),
category_count int,
Spend decimal(14,2),
Exposure decimal(14,2),
key member (member(12))
);

insert into comingling_members
select member_uniq_id, 
       count(distinct category), 
       sum(amt_paid), 
       sum(amt_allowed)
from report_claims
group by member_uniq_id;
      
drop table if exists comingling_metrics;
create table comingling_metrics
(
category_count int,
memberCount int,
totalPaid decimal(14,2),
lob varchar(55),
rolling_year varchar(4),
key lob (lob(10))
);      
      
-- by category count all LOB all time
insert into comingling_metrics
select category_count,
       count(distinct a.member_uniq_id),
       sum(a.amt_paid), 
       'All',
       'All'
from medical_claims_wide a
  join comingling_members b
    on a.member_uniq_id = b.member
where date_of_service between @year_1_start and @year_3_end
group by category_count,
         'All',
         'All';

-- by category count and year, all LOB
insert into comingling_metrics
select category_count,
       count(distinct a.member_uniq_id),
       sum(a.amt_paid), 
       'All',
	   case when a.date_of_service between @year_1_start and @year_1_end then 2016
            when a.date_of_service between @year_2_start and @year_2_end then 2017
            when a.date_of_service between @year_3_start and @year_3_end then 2018 end as rolling_year
from medical_claims_wide a
  join comingling_members b
    on a.member_uniq_id = b.member
where date_of_service between @year_1_start and @year_3_end
group by category_count,
         'All',
	     case when a.date_of_service between @year_1_start and @year_1_end then 2016
              when a.date_of_service between @year_2_start and @year_2_end then 2017
              when a.date_of_service between @year_3_start and @year_3_end then 2018 end 
;

-- by category count, lob, and year
insert into comingling_metrics
select category_count,
       count(distinct a.member_uniq_id),
       sum(a.amt_paid), 
       lob,
	   case when a.date_of_service between @year_1_start and @year_1_end then 2016
            when a.date_of_service between @year_2_start and @year_2_end then 2017
            when a.date_of_service between @year_3_start and @year_3_end then 2018 end as rolling_year
from medical_claims_wide a
  join comingling_members b
    on a.member_uniq_id = b.member
where date_of_service between @year_1_start and @year_3_end
group by category_count,
         lob,
	     case when a.date_of_service between @year_1_start and @year_1_end then 2016
              when a.date_of_service between @year_2_start and @year_2_end then 2017
              when a.date_of_service between @year_3_start and @year_3_end then 2018 end ;

-- by LOB and category count, all years
insert into comingling_metrics
select category_count,
       count(distinct a.member_uniq_id),
       sum(a.amt_paid), 
       lob, 
       'All'
from medical_claims_wide a
  join comingling_members b
    on a.member_uniq_id = b.member
where date_of_service between @year_1_start and @year_3_end
group by category_count,
         lob, 
         'All';

/* Comingling metrics with comingled condition listed */
drop table if exists comingling_members_conditions;
create table comingling_members_conditions
(
conditions varchar(255),
condition_count int,
member varchar(22),
spend decimal(14,2),
exposure decimal(14,2),
key member (member(12))
);

insert into comingling_members_conditions
select group_concat(distinct category order by category separator ';') as conditions,
       count(distinct category) as condition_count,
       member_uniq_id as member,
       sum(amt_paid) as spend,
       sum(amt_allowed) as exposure
from report_claims
group by member_uniq_id;

drop table if exists comingling_members_conditions_metrics;
create table comingling_members_conditions_metrics
(
conditions varchar(255),
condition_count int,
memberCount int,
totalSpend decimal(14,2),
lob varchar(55),
rolling_year varchar(4)
);

/* this query will return the total spend cost by each rolling year, BUT the conditions indicate the total things suffered by the member over hte entire lookback period, 
not that year.  So if somone has 3 lifetime conditions (MD, SCI, OTher), over 2 years (MD, SCI 2017; Other 2018), that person will be bucketed in the 
(MD, SCI, Other) bucket for each year, but the members spend will be split up into the appropriate year based on DOS
*/
 
 -- by year
insert into comingling_members_conditions_metrics
select conditions,
       condition_count,
	   count(distinct member) as memberCount,
       sum(amt_paid) as spend,
       'All' as lob,
       'All' as rolling_year
from medical_claims_wide a
  join comingling_members_conditions b
    on a.member_uniq_id = b.member
where date_of_service between @year_1_start and @year_3_end
group by conditions,
		 condition_count;
              
insert into comingling_members_conditions_metrics
select conditions,
       condition_count,
	   count(distinct member) as memberCount,
       sum(amt_paid) as spend,
       'All' as lob,
       case when a.date_of_service between @year_1_start and @year_1_end then 2016
            when a.date_of_service between @year_2_start and @year_2_end then 2017
            when a.date_of_service between @year_3_start and @year_3_end then 2018 end as rolling_year
from medical_claims_wide a
  join comingling_members_conditions b
    on a.member_uniq_id = b.member
where date_of_service between @year_1_start and @year_3_end
group by case when a.date_of_service between @year_1_start and @year_1_end then 2016
              when a.date_of_service between @year_2_start and @year_2_end then 2017
              when a.date_of_service between @year_3_start and @year_3_end then 2018 end,
              conditions,
              condition_count;
   
-- by year and LOB

insert into comingling_members_conditions_metrics
select conditions,
       condition_count,
	   count(distinct member) as memberCount,
       sum(amt_paid) as spend,
       lob,
       case when a.date_of_service between @year_1_start and @year_1_end then 2016
            when a.date_of_service between @year_2_start and @year_2_end then 2017
            when a.date_of_service between @year_3_start and @year_3_end then 2018 end as rolling_year
from medical_claims_wide a
  join comingling_members_conditions b
    on a.member_uniq_id = b.member
where date_of_service between @year_1_start and @year_3_end
group by case when a.date_of_service between @year_1_start and @year_1_end then 2016
              when a.date_of_service between @year_2_start and @year_2_end then 2017
              when a.date_of_service between @year_3_start and @year_3_end then 2018 end,
              conditions,
              condition_count,
              lob;

-- --------------------------------------------------------------
/* 

RX Profile pieces

*/

-- ----------------------------------------------------------------


-- raw data
drop table if exists rx_metrics;
create table rx_metrics
(
memberCount int,
claimsCount int,
totalSpend decimal(14,2),
lob varchar(55),
rolling_year varchar(4),
cohort varchar(255)
);

-- all Members
insert into rx_metrics
select count(distinct member_uniq_id) as members,
       count(distinct a.claim_number) as claims,
	   sum(amt_paid) as spend,
       'All',
       'All',
       'All Members'
from rx_claims a
where date_filled between @year_1_start and @year_3_end;

insert into rx_metrics
select count(distinct member_uniq_id) as members,
       count(distinct a.claim_number) as claims,
	   sum(amt_paid) as spend,
       lob,
       'All',
       'All Members'
from rx_claims a
where date_filled between @year_1_start and @year_3_end
group by lob,
       'All Members';

insert into rx_metrics
select count(distinct member_uniq_id) as members,
       count(distinct a.claim_number) as claims,
	   sum(amt_paid) as spend,
       lob,
       case when a.date_filled between @year_1_start and @year_1_end then 2016
            when a.date_filled between @year_2_start and @year_2_end then 2017
            when a.date_filled between @year_3_start and @year_3_end then 2018 end as rolling_year,
       'All Members'
from rx_claims a
where date_filled between @year_1_start and @year_3_end
group by lob,
         case when a.date_filled between @year_1_start and @year_1_end then 2016
              when a.date_filled between @year_2_start and @year_2_end then 2017
              when a.date_filled between @year_3_start and @year_3_end then 2018 end,		
         'All Members';

-- Qualified Member RX Spend

-- Add filters to where clauses below to drill down to better qualify member cohort as needed
insert into rx_metrics
select count(distinct member_uniq_id) as members,
       count(distinct a.claim_number) as claims,
	   sum(amt_paid) as spend,
       'All',
       'All',
       'Qualified Members'
from rx_claims a
  join comingling_members_conditions b
    on a.member_uniq_id = b.member
where date_filled between @year_1_start and @year_3_end
 -- and a.lob = 'PP' and rolling_year = 2018;
;

-- Qualified Member RX Spend by year
insert into rx_metrics
select count(distinct member_uniq_id) as members,
       count(distinct a.claim_number) as claims,
	   sum(amt_paid) as spend,
       'All',
       case when a.date_filled between @year_1_start and @year_1_end then 2016
            when a.date_filled between @year_2_start and @year_2_end then 2017
            when a.date_filled between @year_3_start and @year_3_end then 2018 end as rolling_year,
	   'Qualified Members'
from rx_claims a
  join comingling_members_conditions b
    on a.member_uniq_id = b.member
where date_filled between @year_1_start and @year_3_end
-- and a.lob = 'PP' and rolling_year = 2018
group by case when a.date_filled between @year_1_start and @year_1_end then 2016
              when a.date_filled between @year_2_start and @year_2_end then 2017
              when a.date_filled between @year_3_start and @year_3_end then 2018 end,
	     'All',
         'Qualified Members';
         
-- Qualified Member RX Spend by lob and year
insert into rx_metrics
select count(distinct member_uniq_id) as members,
       count(distinct a.claim_number) as claims,
	   sum(amt_paid) as spend,
       lob,
       case when a.date_filled between @year_1_start and @year_1_end then 2016
            when a.date_filled between @year_2_start and @year_2_end then 2017
            when a.date_filled between @year_3_start and @year_3_end then 2018 end as rolling_year,
	   'Qualified Members'
from rx_claims a
  join comingling_members_conditions b
    on a.member_uniq_id = b.member
where date_filled between @year_1_start and @year_3_end
-- and a.lob = 'PP' and rolling_year = 2018
group by case when a.date_filled between @year_1_start and @year_1_end then 2016
              when a.date_filled between @year_2_start and @year_2_end then 2017
              when a.date_filled between @year_3_start and @year_3_end then 2018 end,
	     lob,
         'Qualified Members';

-- Qualified Member RX Spend by lob 
insert into rx_metrics
select count(distinct member_uniq_id) as members,
       count(distinct a.claim_number) as claims,
	   sum(amt_paid) as spend,
       lob,
       'All',
	   'Qualified Members'
from rx_claims a
  join comingling_members_conditions b
    on a.member_uniq_id = b.member
where date_filled between @year_1_start and @year_3_end
-- and a.lob = 'PP' and rolling_year = 2018
group by lob,
         'All',
         'Qualified Members';


--  RX Spend by Condition
drop table if exists temp_rx_member_cat;
create table temp_rx_member_cat
(
Member varchar(55),
category varchar(55),
lob varchar(55),
key member (member(12)),
key category (category(55)),
key lob (lob(55))
);

insert into temp_rx_member_cat
select distinct member_uniq_id,
                category,
                lob
from report_claims;

drop table if exists rx_condition_metrics;
create table rx_condition_metrics
(
category varchar(255),
memberCount int,
totalPaid decimal(14,2),
lob varchar(55),
rolling_year varchar(4),
cohort varchar(255)
);

-- Qualified members, lob, and years by condition
insert into rx_condition_metrics
select category,
       count(distinct a.member_uniq_id) as MemberCount,
	   sum(a.amt_paid) as TotalPaid,
       'All',
       'All',
	   'Qualified Members'
from rx_claims a
  join temp_rx_member_cat b
    on a.member_uniq_id=b.member
where  date_filled between @year_1_start and @year_3_end
group by b.category,
         'All',
		 'All',
	     'Qualified Members';

-- all members, lobs by year
insert into rx_condition_metrics
select category,
       count(distinct a.member_uniq_id) as MemberCount,
	   sum(a.amt_paid) as TotalPaid,
       'All',
       case when a.date_filled between @year_1_start and @year_1_end then 2016
            when a.date_filled between @year_2_start and @year_2_end then 2017
            when a.date_filled between @year_3_start and @year_3_end then 2018 end as rolling_year,
	  'Qualified Members'
from rx_claims a
  join temp_rx_member_cat b
    on a.member_uniq_id=b.member
where  date_filled between @year_1_start and @year_3_end
group by b.category,
         'All',
		 case when a.date_filled between @year_1_start and @year_1_end then 2016
            when a.date_filled between @year_2_start and @year_2_end then 2017
            when a.date_filled between @year_3_start and @year_3_end then 2018 end,
	     'Qualified Members';

-- all members by year and lob
insert into rx_condition_metrics
select category,
       count(distinct a.member_uniq_id) as MemberCount,
	   sum(a.amt_paid) as TotalPaid,
       b.lob,
       case when a.date_filled between @year_1_start and @year_1_end then 2016
            when a.date_filled between @year_2_start and @year_2_end then 2017
            when a.date_filled between @year_3_start and @year_3_end then 2018 end as rolling_year,
	  'Qualified Members'
from rx_claims a
  join temp_rx_member_cat b
    on a.member_uniq_id=b.member
where  date_filled between @year_1_start and @year_3_end
group by b.category,
         b.lob,
		 case when a.date_filled between @year_1_start and @year_1_end then 2016
            when a.date_filled between @year_2_start and @year_2_end then 2017
            when a.date_filled between @year_3_start and @year_3_end then 2018 end,
	     'Qualified Members';

-- all members all years by lob
insert into rx_condition_metrics
select category,
       count(distinct a.member_uniq_id) as MemberCount,
	   sum(a.amt_paid) as TotalPaid,
       b.lob,
       'All',
	   'Qualified Members'
from rx_claims a
  join temp_rx_member_cat b
    on a.member_uniq_id=b.member
where  date_filled between @year_1_start and @year_3_end
group by b.category,
         b.lob,
		 'All',
	     'Qualified Members';

-- Qualified Member RX Spend by comingling count ----------------------------------------

-- all members, lob, and years
insert into rx_condition_metrics
select category_count,
       count(distinct a.member_uniq_id) as MemberCount,
	   sum(a.amt_paid) as TotalPaid,
       'All',
       'All',
	   'Qualified Members'
from rx_claims a
  join comingling_members b
    on a.member_uniq_id = b.member
where  date_filled between @year_1_start and @year_3_end
group by category_count,
         'All',
		 'All',
	     'Qualified Members';


-- all members, lobs by year
insert into rx_condition_metrics
select category_count,
       count(distinct a.member_uniq_id) as MemberCount,
	   sum(a.amt_paid) as TotalPaid,
       'All',
       case when a.date_filled between @year_1_start and @year_1_end then 2016
            when a.date_filled between @year_2_start and @year_2_end then 2017
            when a.date_filled between @year_3_start and @year_3_end then 2018 end as rolling_year,
	  'Qualified Members'
from rx_claims a
  join comingling_members b
    on a.member_uniq_id = b.member
where  date_filled between @year_1_start and @year_3_end
group by category_count,
         'All',
		 case when a.date_filled between @year_1_start and @year_1_end then 2016
            when a.date_filled between @year_2_start and @year_2_end then 2017
            when a.date_filled between @year_3_start and @year_3_end then 2018 end,
	     'Qualified Members';

-- all members by year and lob
insert into rx_condition_metrics
select category_count,
       count(distinct a.member_uniq_id) as MemberCount,
	   sum(a.amt_paid) as TotalPaid,
       lob,
       case when a.date_filled between @year_1_start and @year_1_end then 2016
            when a.date_filled between @year_2_start and @year_2_end then 2017
            when a.date_filled between @year_3_start and @year_3_end then 2018 end as rolling_year,
	  'Qualified Members'
from rx_claims a
  join comingling_members b
    on a.member_uniq_id = b.member
where  date_filled between @year_1_start and @year_3_end
group by category_count,
         lob,
		 case when a.date_filled between @year_1_start and @year_1_end then 2016
            when a.date_filled between @year_2_start and @year_2_end then 2017
            when a.date_filled between @year_3_start and @year_3_end then 2018 end,
	     'Qualified Members';

-- all members all years by lob
insert into rx_condition_metrics
select category_count,
       count(distinct a.member_uniq_id) as MemberCount,
	   sum(a.amt_paid) as TotalPaid,
       lob,
       'All',
	   'Qualified Members'
from rx_claims a
  join comingling_members b
    on a.member_uniq_id = b.member
where  date_filled between @year_1_start and @year_3_end
group by category_count,
         lob,
		 'All',
	     'Qualified Members';

-- ------------------------------------------
/* 

Single Condition member category analysis 

*/
-- -----------------------------------------------

drop table if exists single_condition_member_metrics_by_category;
create table single_condition_member_metrics_by_category
(
category varchar(55),
memberCount int,
totalPaid decimal(14,2),
lob varchar(55),
rolling_year varchar(4),
key category (category(12)),
key lob (lob(10))
);

insert into single_condition_member_metrics_by_category
select category,
       count(distinct member_uniq_id) as MemberCount,
	   sum(amt_paid) as TotalPaid,
       'All',
       'All'
from report_claims a
  join comingling_members b
    on a.member_uniq_id = b.Member
where date_of_service between @year_1_start and @year_3_end
  and category_count = 1
group by category
order by category;

insert into single_condition_member_metrics_by_category
select category,
       count(distinct member_uniq_id) as MemberCount,
	   sum(amt_paid) as TotalPaid,
       lob,
	   'All'
from report_claims a
  join comingling_members b
    on a.member_uniq_id = b.Member
where date_of_service between @year_1_start and @year_3_end
  and category_count = 1
group by lob, 
		 category
order by lob, category;

insert into single_condition_member_metrics_by_category
select category,
       count(distinct member_uniq_id) as MemberCount,
	   sum(amt_paid) as TotalPaid,
       lob,
	   rolling_year
from report_claims a
  join comingling_members b
    on a.member_uniq_id = b.Member
where date_of_service between @year_1_start and @year_3_end
  and category_count = 1
group by lob, 
		 category,
         rolling_year
order by lob, rolling_year, category; 


drop table if exists single_condition_rx_by_condition;
create table single_condition_rx_by_condition
(
category varchar(255),
memberCount int,
totalPaid decimal(14,2),
lob varchar(55),
rolling_year varchar(4),
cohort varchar(255)
);


-- all members, lob, and years
insert into single_condition_rx_by_condition
select conditions,
       count(distinct a.member_uniq_id) as MemberCount,
	   sum(a.amt_paid) as TotalPaid,
       'All',
       'All',
	   'Single Condition Members'
from rx_claims a
  join comingling_members_conditions b
    on a.member_uniq_id = b.member
where  date_filled between @year_1_start and @year_3_end
  and condition_count = 1
group by conditions,
         'All',
		 'All',
	     'Single Condition Members';

-- all members, lobs by year
insert into single_condition_rx_by_condition
select conditions,
       count(distinct a.member_uniq_id) as MemberCount,
	   sum(a.amt_paid) as TotalPaid,
       'All',
       case when a.date_filled between @year_1_start and @year_1_end then 2016
            when a.date_filled between @year_2_start and @year_2_end then 2017
            when a.date_filled between @year_3_start and @year_3_end then 2018 end as rolling_year,
	  'Single Condition Members'
from rx_claims a
  join comingling_members_conditions b
    on a.member_uniq_id = b.member
where  date_filled between @year_1_start and @year_3_end
  and condition_count = 1
group by conditions,
         'All',
		 case when a.date_filled between @year_1_start and @year_1_end then 2016
            when a.date_filled between @year_2_start and @year_2_end then 2017
            when a.date_filled between @year_3_start and @year_3_end then 2018 end,
	     'Single Condition Members';

-- all members by year and lob
insert into single_condition_rx_by_condition
select conditions,
       count(distinct a.member_uniq_id) as MemberCount,
	   sum(a.amt_paid) as TotalPaid,
       lob,
       case when a.date_filled between @year_1_start and @year_1_end then 2016
            when a.date_filled between @year_2_start and @year_2_end then 2017
            when a.date_filled between @year_3_start and @year_3_end then 2018 end as rolling_year,
	  'Single Condition Members'
from rx_claims a
  join comingling_members_conditions b
    on a.member_uniq_id = b.member
where  date_filled between @year_1_start and @year_3_end
  and condition_count = 1
group by conditions,
         lob,
		 case when a.date_filled between @year_1_start and @year_1_end then 2016
            when a.date_filled between @year_2_start and @year_2_end then 2017
            when a.date_filled between @year_3_start and @year_3_end then 2018 end,
	     'Single Condition Members';

-- all members all years by lob
insert into single_condition_rx_by_condition
select conditions,
       count(distinct a.member_uniq_id) as MemberCount,
	   sum(a.amt_paid) as TotalPaid,
       lob,
       'All',
	   'Single Condition Members'
from rx_claims a
  join comingling_members_conditions b
    on a.member_uniq_id = b.member
where  date_filled between @year_1_start and @year_3_end
  and condition_count = 1
group by conditions,
         lob,
		 'All',
	     'Single Condition Members';

-- ---------------------------------------------------------------------------
    
/*

 Utilization by POS

*/

-- -------------------------------------------------------------------------

drop table if exists pos_utilization_metrics;
create table pos_utilization_metrics
(
lob varchar(55),
category_desc_or_count varchar(55),
pos_description varchar(255),
totalSpend decimal(14,2),
memberCount int,
rolling_year varchar(4),
cohort varchar(255),
admit_ind varchar(1)
);

-- all members
insert into pos_utilization_metrics
select lob,
       'All',
       c.name,
       sum(a.amt_paid), 
       count(distinct a.member_uniq_id),
	   case when a.date_of_service between @year_1_start and @year_1_end then 2016
            when a.date_of_service between @year_2_start and @year_2_end then 2017
            when a.date_of_service between @year_3_start and @year_3_end then 2018 end as rolling_year,
	   'All Members',
       admit_ind
from medical_claims_wide a
  left join resources.pos c
    on a.cms_place_of_service_id = c.pos
where date_of_service between @year_1_start and @year_3_end
group by 'All',
         lob,
         c.name,
	     case when a.date_of_service between @year_1_start and @year_1_end then 2016
              when a.date_of_service between @year_2_start and @year_2_end then 2017
              when a.date_of_service between @year_3_start and @year_3_end then 2018 end,
		 'All Members',
         admit_ind;

-- comingling members
insert into pos_utilization_metrics
select lob,
       category_count,
       c.name,
       sum(a.amt_paid), 
       count(distinct a.member_uniq_id),
	   case when a.date_of_service between @year_1_start and @year_1_end then 2016
            when a.date_of_service between @year_2_start and @year_2_end then 2017
            when a.date_of_service between @year_3_start and @year_3_end then 2018 end as rolling_year,
	   'Qualified Members',
       admit_ind
from medical_claims_wide a
  join comingling_members b
    on a.member_uniq_id = b.member
  left join resources.pos c
    on a.cms_place_of_service_id = c.pos
where date_of_service between @year_1_start and @year_3_end
group by category_count,
         lob,
         c.name,
	     case when a.date_of_service between @year_1_start and @year_1_end then 2016
              when a.date_of_service between @year_2_start and @year_2_end then 2017
              when a.date_of_service between @year_3_start and @year_3_end then 2018 end,
		'Qualified Members',
         admit_ind;



-- yearly member spend
drop table if exists year_member_spend_all;
create table year_member_spend_all
(
memberID varchar(255),
totalPaid2016 decimal(14,2),
totalPaid2017 decimal(14,2),
totalPaid2018 decimal(14,2),
lob varchar(55),
key memberID (memberID(20))
);

insert into year_member_spend_all
select mem.memberID,
       ifnull(md16.totalPaid,0) + ifnull(rx16.TotalPaid,0) as totalPaid2016,
       ifnull(md17.totalPaid,0) + ifnull(rx17.TotalPaid,0) as totalPaid2017,
       ifnull(md18.totalPaid,0) + ifnull(rx18.TotalPaid,0) as totalPaid2018,
       null
from 
  (select distinct member as memberID
   from comingling_members
  ) mem
left join
  (select member_uniq_id as memberID,
          sum(amt_paid) as totalPaid
   from medical_claims_wide a
     join comingling_members b
       on a.member_uniq_id = b.member
   where date_of_service between @year_1_start and @year_1_end
   group by a.member_uniq_id) md16
on mem.memberID = md16.memberID
left join
  (select member_uniq_id as memberID,
          sum(amt_paid) as totalPaid
   from rx_claims a
     join comingling_members b
       on a.member_uniq_id = b.member
   where date_filled between @year_1_start and @year_1_end
   group by a.member_uniq_id) rx16
on mem.memberID = rx16.memberID
left join
  (select member_uniq_id as memberID,
          sum(amt_paid) as totalPaid
   from medical_claims_wide a
     join comingling_members b
       on a.member_uniq_id = b.member
   where date_of_service between @year_2_start and @year_2_end
   group by a.member_uniq_id) md17
on mem.memberID = md17.memberID
left join
  (select member_uniq_id as memberID,
          sum(amt_paid) as totalPaid
   from rx_claims a
     join comingling_members b
       on a.member_uniq_id = b.member
   where date_filled between @year_2_start and @year_2_end
   group by a.member_uniq_id) rx17
on mem.memberID = rx17.memberID
left join
  (select member_uniq_id as memberID,
          sum(amt_paid) as totalPaid
   from medical_claims_wide a
     join comingling_members b
       on a.member_uniq_id = b.member
   where date_of_service between @year_3_start and @year_3_end
   group by a.member_uniq_id) md18
on mem.memberID = md18.memberID
left join
  (select member_uniq_id as memberID,
          sum(amt_paid) as totalPaid
   from rx_claims a
     join comingling_members b
       on a.member_uniq_id = b.member
   where date_filled between @year_3_start and @year_3_end
   group by a.member_uniq_id) rx18
on mem.memberID = rx18.memberID;

update year_member_spend_all a, eligibility_wide b
set a.lob = b.lob
where a.memberID = b.member_uniq_id
 -- and b.active_record = 1 not set for Tufts
 ;