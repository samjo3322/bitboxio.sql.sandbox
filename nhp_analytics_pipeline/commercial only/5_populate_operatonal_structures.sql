/* Create Diagnosis Long Table */ 
drop table if exists member_dx_details_long;
CREATE TABLE `member_dx_details_long` (
  `member_uniq_id` varchar(255) DEFAULT NULL,
  lob varchar(255) default null,
  `icd_code_from_claim` varchar(20) DEFAULT NULL,
  `code_description` text,
  `claim_number` varchar(255) DEFAULT NULL,
  `date_of_service` date DEFAULT NULL,
  `isPrimary`   int(1) DEFAULT NULL,
  `isSecondary` int(1) DEFAULT NULL,
  `isACSC`      int(1) DEFAULT NULL,
  `chronicity` varchar(255) DEFAULT NULL,
  `category` varchar(255) default null,
  `savings_category` varchar(255) default null,
  `savings_factor` decimal(14,2) default null,
  `joined` int default 0,
  KEY `member_uniq_id` (`member_uniq_id`),
  KEY `icd` (`icd_code_from_claim`),
  KEY `claim_number` (`claim_number`),
  KEY `date` (`date_of_service`),
  KEY `isPrimary` (`isPrimary`),
  KEY `isSecondary` (`isSecondary`),
  key category (category(2)),
  key savings_category (savings_category(2))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- truncate table member_dx_details_long;
insert into member_dx_details_long
(member_uniq_id,lob,icd_code_from_claim,claim_number, date_of_service)
(select distinct member_uniq_id,
				lob,
                icd_1,
                claim_number,
                date_of_service
from medical_claims_wide
where ifnull(icd_1,'') != ''
)
union
(select distinct member_uniq_id,
                 lob,
                 icd_2,
                claim_number,
                date_of_service
from medical_claims_wide
where ifnull(icd_2,'') != ''
)
union
(select distinct member_uniq_id,
				lob,
                icd_3,
                claim_number,
                date_of_service
from medical_claims_wide
where ifnull(icd_3,'') != ''
)
union
(select distinct member_uniq_id,
                lob,
                icd_4,
                claim_number,
                date_of_service
from medical_claims_wide
where ifnull(icd_4,'') != ''
)
union
(select distinct member_uniq_id,
                icd_5,
                lob,
                claim_number,
                date_of_service
from medical_claims_wide
where ifnull(icd_5,'') != ''
)
union
(select distinct member_uniq_id,
                lob,
                icd_6,
                claim_number,
                date_of_service
from medical_claims_wide
where ifnull(icd_6,'') != ''
)
union
(select distinct member_uniq_id,
                lob,
                icd_7,
                claim_number,
                date_of_service
from medical_claims_wide
where ifnull(icd_7,'') != ''
)
union
(select distinct member_uniq_id,
                lob,
				icd_8,
                claim_number,
                date_of_service
from medical_claims_wide
where ifnull(icd_8,'') != ''
)
union
(select distinct member_uniq_id,
                lob,
                icd_9,
                claim_number,
                date_of_service
from medical_claims_wide
where ifnull(icd_9,'') != ''
)
union
(select distinct member_uniq_id,
                lob,
                icd_10,
                claim_number,
                date_of_service
from medical_claims_wide
where ifnull(icd_10,'') != ''
);

update member_dx_details_long a
set isPrimary = 1
where a.icd_code_from_claim in (select icd_10 from resources.uh_dx_icd_final where is_primary = 1);

update member_dx_details_long a
set isPrimary = 1
where a.icd_code_from_claim in (select icd_9 from resources.uh_dx_icd_final where is_primary = 1);

update member_dx_details_long a
set isACSC = 1
where a.icd_code_from_claim in (select icd_10 from resources.uh_dx_icd_final where is_ACSC = 1);

update member_dx_details_long a
set isACSC = 1
where a.icd_code_from_claim in (select icd_9 from resources.uh_dx_icd_final where is_ACSC = 1);

update member_dx_details_long a
set isSecondary = 1
where a.icd_code_from_claim in (select icd_10 from resources.uh_dx_icd_final where is_secondary = 1);

update member_dx_details_long a
set isSecondary = 1
where a.icd_code_from_claim in (select icd_9 from resources.uh_dx_icd_final where is_secondary = 1);

update member_dx_details_long a, resources.uh_dx_icd_final b
set a.chronicity = b.chronicity
where (a.icd_code_from_claim = b.icd_10);

update member_dx_details_long a, resources.uh_dx_icd_final b
set a.chronicity = b.chronicity
where (a.icd_code_from_claim = b.icd_9);

update member_dx_details_long a, resources.uh_dx_icd_final b
set a.code_description = b.code_description
where (a.icd_code_from_claim = b.icd_10);

update member_dx_details_long a, resources.uh_dx_icd_final b
set a.code_description = b.code_description
where (a.icd_code_from_claim = b.icd_9);

update member_dx_details_long a, resources.uh_dx_icd_final b
set a.category = b.category
where (a.icd_code_from_claim = b.icd_10);

update member_dx_details_long a, resources.uh_dx_icd_final b
set a.category = b.category
where (a.icd_code_from_claim = b.icd_9);

update member_dx_details_long a, resources.uh_dx_icd_final b
set a.savings_category = b.savings_category
where (a.icd_code_from_claim = b.icd_10);

update member_dx_details_long a, resources.uh_dx_icd_final b
set a.savings_category = b.savings_category
where (a.icd_code_from_claim = b.icd_9);

update member_dx_details_long a, resources.uh_dx_icd_final b
set a.savings_factor = b.savings_factor
where (a.icd_code_from_claim = b.icd_10);

update member_dx_details_long a, resources.uh_dx_icd_final b
set a.savings_factor = b.savings_factor
where (a.icd_code_from_claim = b.icd_9);

update member_dx_details_long a, resources.uh_dx_icd_final b
set a.joined = 1
where (a.icd_code_from_claim = b.icd_10);

update member_dx_details_long a, resources.uh_dx_icd_final b
set a.joined = 1
where (a.icd_code_from_claim = b.icd_9);

/* Set up member filters */

drop table if exists members_spend;
create table members_spend
(
member_uniq_id varchar(55),
amt_paid_med decimal(14,2),
amt_allowed_med decimal(14,2),
amt_paid_rx decimal(14,2),
amt_allowed_rx decimal(14,2),
totalPaid decimal(14,2),
totalAllowed decimal(14,2),
key member (member_uniq_id(11))
);

insert into members_spend
select med.member_uniq_id,
       med.paid,
       med.allowed,
       rx.paid,
       rx.allowed,
       med.paid+rx.paid as totalPaid,
       med.allowed+rx.allowed as totalAllowed
from
  (select member_uniq_id, 
         sum(amt_paid) as paid, 
         sum(amt_allowed) as allowed
  from medical_claims_wide
  group by member_uniq_id) med
join
  (select member_uniq_id, 
         sum(amt_paid) as paid,
         sum(amt_allowed) as allowed
   from rx_claims
   group by member_uniq_id) rx
on med.member_uniq_id = rx.member_uniq_id;

alter table members_spend
add column member_type varchar(55) default 'Not Qualified';

drop table if exists members_spend_filter;
create table members_spend_filter
(
member_uniq_id varchar(55),
totalPaid decimal(14,2),
totalAllowed decimal(14,2),
key member (member_uniq_id(11))
);

truncate table members_spend_filter;
insert into members_spend_filter
select member_uniq_id,
       totalPaid,
       totalAllowed
from members_spend
where TotalPaid >= 10000;