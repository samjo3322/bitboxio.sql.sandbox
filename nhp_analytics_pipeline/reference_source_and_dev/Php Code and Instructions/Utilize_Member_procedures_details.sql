drop table if exists mobe_live.member_procedure_details;
CREATE TABLE mobe_live.`member_procedure_details` (
  `member_uniq_id` varchar(255) DEFAULT NULL,
  `procedure_code_from_claim` varchar(20) DEFAULT NULL,
  `procedure_description` text,
  `drg` varchar(3) DEFAULT NULL,
  service_provider_name varchar(255) DEFAULT NULL,
  `claim_number` varchar(255) DEFAULT NULL,
  `service_start_date` datetime DEFAULT NULL,
  KEY `member_uniq_id` (`member_uniq_id`),
  KEY `procedure` (`procedure_code_from_claim`),
  KEY `claim_number` (`claim_number`),
  KEY `date` (`service_start_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

truncate table mobe_live.member_procedure_details;
insert into member_procedure_details
(member_uniq_id, procedure_code_from_claim, drg, service_provider_name,claim_number, service_start_date)
(select distinct member_uniq_id,
                procedure_code,
                drg,
                service_provider_name,
                claim_number,
                service_start_date
from mobe_live.medical_claims_wide
where procedure_code is not null and cms_place_of_service_id = 21)
union
(select distinct member_uniq_id,
                procedure_code_2,
                 drg,
                service_provider_name,
                claim_number,
                service_start_date
from mobe_live.medical_claims_wide
where procedure_code_2 is not null and cms_place_of_service_id = 21)
union
(select distinct member_uniq_id,
                procedure_code_3,
                 drg,
                service_provider_name,
                claim_number,
                service_start_date
from mobe_live.medical_claims_wide
where procedure_code_3 is not null and cms_place_of_service_id = 21)
union
(select distinct member_uniq_id,
                procedure_code_4,
                 drg,
                service_provider_name,
                claim_number,
                service_start_date
from mobe_live.medical_claims_wide
where procedure_code_4 is not null and cms_place_of_service_id = 21)
union
(select distinct member_uniq_id,
                procedure_code_5,
                 drg,
                service_provider_name,
                claim_number,
                service_start_date
from mobe_live.medical_claims_wide
where procedure_code_5 is not null and cms_place_of_service_id = 21)
union
(select distinct member_uniq_id,
                procedure_code_6,
                drg,
                service_provider_name,
                claim_number,
                service_start_date
from mobe_live.medical_claims_wide
where procedure_code_6 is not null and cms_place_of_service_id = 21)
union
(select distinct member_uniq_id,
                procedure_code_7,
                 drg,
                service_provider_name,
                claim_number,
                service_start_date
from mobe_live.medical_claims_wide
where procedure_code_7 is not null and cms_place_of_service_id = 21)
union
(select distinct member_uniq_id,
                procedure_code_8,
                 drg,
                service_provider_name,
                claim_number,
                service_start_date
from mobe_live.medical_claims_wide
where procedure_code_8 is not null and cms_place_of_service_id = 21)
union
(select distinct member_uniq_id,
                procedure_code_9,
                 drg,
                service_provider_name,
                claim_number,
                service_start_date
from mobe_live.medical_claims_wide
where procedure_code_9 is not null and cms_place_of_service_id = 21)
union
(select distinct member_uniq_id,
                procedure_code_10,
                drg,
                service_provider_name,
                claim_number,
                service_start_date
from mobe_live.medical_claims_wide
where procedure_code_10 is not null and cms_place_of_service_id = 21);

update mobe_live.member_dx_details_2 a
set isPrimary = 1
where a.icd_code_from_claim in (select icd_10 from resources.uh_dx_icd_final where `Primary` = 1);

update mobe_live.member_dx_details_2 a
set isPrimary = 1
where a.icd_code_from_claim in (select icd_9 from resources.uh_dx_icd_final where `Primary` = 1);

update mobe_live.member_dx_details_2 a
set isACSC = 1
where a.icd_code_from_claim in (select icd_10 from resources.uh_dx_icd_final where is_ACSC = 1);

update mobe_live.member_dx_details_2 a
set isACSC = 1
where a.icd_code_from_claim in (select icd_9 from resources.uh_dx_icd_final where is_ACSC = 1);

update mobe_live.member_dx_details_2 a
set isSecondary = 1
where a.icd_code_from_claim in (select icd_10 from resources.uh_dx_icd_final where catagory = 'Secondary');

update mobe_live.member_dx_details_2 a
set isSecondary = 1
where a.icd_code_from_claim in (select icd_9 from resources.uh_dx_icd_final where catagory = 'Secondary');

update mobe_live.member_procedure_details a, resources.uh_dx_icd_final b
set a.episode_of_care_description = b.episode_of_care_description
where (a.icd_code_from_claim = b.icd_10);

update mobe_live.member_dx_details_2 a, resources.uh_dx_icd_final b
set a.episode_of_care_description = b.episode_of_care_description
where (a.icd_code_from_claim = b.icd_9);


update mobe_live.member_dx_details_2 a, resources.uh_dx_icd_final b
set a.code_description = b.code_description
where (a.icd_code_from_claim = b.icd_10);

update mobe_live.member_dx_details_2 a, resources.uh_dx_icd_final b
set a.code_description = b.code_description
where (a.icd_code_from_claim = b.icd_9);