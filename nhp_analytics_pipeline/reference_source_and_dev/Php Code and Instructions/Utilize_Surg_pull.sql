select
MemberId,
PatientName,
ProviderName,
ProcedureDescription,
Max(ServiceDate) as ServiceDate
from mobe_live.report_surg_1
group by MemberId, PatientName, ProviderName, ProcedureDescription
order by MemberId asc, ServiceDate desc