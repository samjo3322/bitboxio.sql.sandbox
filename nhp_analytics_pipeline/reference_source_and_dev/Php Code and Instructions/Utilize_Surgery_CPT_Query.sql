truncate mobe_live.report_surg;

insert into mobe_live.report_surg

select  distinct
        a.member_uniq_id as MemberId, 
        a.group_id_1 as InsuranceGroup, 
        a.group_id_2 as InsuranceGroupPlan, 
ifnull(concat(c.patient_last_name,', ',c.patient_first_name), concat(a.patient_last_name,', ',a.patient_first_name)) as PatientName,
        a.service_provider_name as ProviderName,
        a.date_of_service as ServiceDate,
        a.procedure_code as ProcedureCode,
        b.description as  ProcedureDescription
from mobe_live.medical_claims_wide a 
inner join resources.cpt_common b 
	on procedure_code = Code
inner join mobe_live.eligibility_wide c 
	on a.member_uniq_id = c.member_uniq_id 
		and c.control_group = 0
		and upper(c.group_id_prefix) in ('BCBSTX', 'BCBSIL')

where fully_adjudicated = 1
	and procedure_code between 10021 and 69990
	and cms_place_of_service_id = 21

