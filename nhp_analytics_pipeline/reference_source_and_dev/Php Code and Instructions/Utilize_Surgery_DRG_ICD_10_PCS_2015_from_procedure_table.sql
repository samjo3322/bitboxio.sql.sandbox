insert into mobe_live.report_surg

select  distinct
        a.member_uniq_id as MemberId, 
      concat(d.patient_last_name,', ',d.patient_first_name) as PatientName,        
        a.service_provider_name as ProviderName,
        a.service_start_date as ServiceDate,
        procedure_code_from_claim as ProcedureCode,
        c.short_description as ProcedureDescription
from mobe_live.member_procedure_details a 
inner join resources.drg b 	
	on a.drg = b.MS_DRG 
inner join resources.icd10_proc_2015 c 
	on a.procedure_code_from_claim = c.procedure_code
inner join mobe_live.eligibility_wide d 
	on a.member_uniq_id = d.member_uniq_id 
		and d.control_group = 0
		and upper(d.group_id_prefix) in ('BCBSTX', 'BCBSIL')
where   
    year(a.service_start_date) = 2015
	and b.TYPE = "SURG"


union

select  distinct
               a.member_uniq_id as MemberId, 
        a.group_id_1 as InsuranceGroup, 
        a.group_id_2 as InsuranceGroupPlan, 
concat(a.patient_last_name,', ',a.patient_first_name) as PatientName,         a.service_provider_name as ProviderName,
        a.date_of_service as ServiceDate,
        procedure_code_3 as ProcedureCode,
    c.short_description as ProcedureDescription
from mobe_live.medical_claims_wide a 
inner join resources.drg b 
	on a.drg = b.MS_DRG 
inner join resources.icd10_proc_2015 c 
	on a.procedure_code_3 = c.procedure_code
where   
    year(a.date_of_service) = 2015
	and cms_place_of_service_id = 21
	and b.TYPE = "SURG"
    and a.member_uniq_id in (select member_uniq_id from mobe_live.eligibility_wide
                                    where control_group = 0 and group_id_prefix in ('BCBSTX', 'BCBSIL'))



union

select  distinct
              a.member_uniq_id as MemberId, 
        a.group_id_1 as InsuranceGroup, 
        a.group_id_2 as InsuranceGroupPlan, 
concat(a.patient_last_name,', ',a.patient_first_name) as PatientName,         a.service_provider_name as ProviderName,
        a.date_of_service as ServiceDate,
        procedure_code_4 as ProcedureCode,
      c.short_description as ProcedureDescription
from mobe_live.medical_claims_wide a 
inner join resources.drg b 
	on a.drg = b.MS_DRG 
inner join resources.icd10_proc_2015 c 
	on a.procedure_code_4 = c.procedure_code
where   
    year(a.date_of_service) = 2015
	and cms_place_of_service_id = 21
	and b.TYPE = "SURG"
    and a.member_uniq_id in (select member_uniq_id from mobe_live.eligibility_wide
                                    where control_group = 0 and group_id_prefix in ('BCBSTX', 'BCBSIL'))



union

select  distinct
                a.member_uniq_id as MemberId, 
        a.group_id_1 as InsuranceGroup, 
        a.group_id_2 as InsuranceGroupPlan, 
concat(a.patient_last_name,', ',a.patient_first_name) as PatientName,        a.service_provider_name as ProviderName,
        a.date_of_service as ServiceDate,
        procedure_code_5 as ProcedureCode,
        c.short_description as ProcedureDescription
from mobe_live.medical_claims_wide a 
inner join resources.drg b 
	on a.drg = b.MS_DRG 
inner join resources.icd10_proc_2015 c 
	on a.procedure_code_5 = c.procedure_code
where   
    year(a.date_of_service) = 2015
	and cms_place_of_service_id = 21
	and b.TYPE = "SURG"
    and a.member_uniq_id in (select member_uniq_id from mobe_live.eligibility_wide
                                    where control_group = 0 and group_id_prefix in ('BCBSTX', 'BCBSIL'))


union

select  distinct
                a.member_uniq_id as MemberId, 
        a.group_id_1 as InsuranceGroup, 
        a.group_id_2 as InsuranceGroupPlan, 
concat(a.patient_last_name,', ',a.patient_first_name) as PatientName,         a.service_provider_name as ProviderName,
        a.date_of_service as ServiceDate,
        procedure_code_6 as ProcedureCode,
         c.short_description as ProcedureDescription
from mobe_live.medical_claims_wide a 
inner join resources.drg b 
	on a.drg = b.MS_DRG 
inner join resources.icd10_proc_2015 c 
	on a.procedure_code_6 = c.procedure_code
where   
    year(a.date_of_service) = 2015
	and cms_place_of_service_id = 21
	and b.TYPE = "SURG"
    and a.member_uniq_id in (select member_uniq_id from mobe_live.eligibility_wide
                                    where control_group = 0 and group_id_prefix in ('BCBSTX', 'BCBSIL'))



union

select  distinct
                a.member_uniq_id as MemberId, 
        a.group_id_1 as InsuranceGroup, 
        a.group_id_2 as InsuranceGroupPlan, 
concat(a.patient_last_name,', ',a.patient_first_name) as PatientName,        a.service_provider_name as ProviderName,
        a.date_of_service as ServiceDate,
        procedure_code_7 as ProcedureCode,
         c.short_description as ProcedureDescription
from mobe_live.medical_claims_wide a 
inner join resources.drg b 
	on a.drg = b.MS_DRG 
inner join resources.icd10_proc_2015 c 
	on a.procedure_code_7 = c.procedure_code
where   
    year(a.date_of_service) = 2015
	and cms_place_of_service_id = 21
	and b.TYPE = "SURG"
    and a.member_uniq_id in (select member_uniq_id from mobe_live.eligibility_wide
                                    where control_group = 0 and group_id_prefix in ('BCBSTX', 'BCBSIL'))





union

select  distinct
                a.member_uniq_id as MemberId, 
        a.group_id_1 as InsuranceGroup, 
        a.group_id_2 as InsuranceGroupPlan, 
concat(a.patient_last_name,', ',a.patient_first_name) as PatientName,     a.service_provider_name as ProviderName,
        a.date_of_service as ServiceDate,
        procedure_code_8 as ProcedureCode,
         c.short_description as ProcedureDescription
from mobe_live.medical_claims_wide a 
inner join resources.drg b 
	on a.drg = b.MS_DRG 
inner join resources.icd10_proc_2015 c 
	on a.procedure_code_8 = c.procedure_code
where   
    year(a.date_of_service) = 2015
	and cms_place_of_service_id = 21
	and b.TYPE = "SURG"
    and a.member_uniq_id in (select member_uniq_id from mobe_live.eligibility_wide
                                    where control_group = 0 and group_id_prefix in ('BCBSTX', 'BCBSIL'))




union

select  distinct
                a.member_uniq_id as MemberId, 
        a.group_id_1 as InsuranceGroup, 
        a.group_id_2 as InsuranceGroupPlan, 
concat(a.patient_last_name,', ',a.patient_first_name) as PatientName,        a.service_provider_name as ProviderName,
        a.date_of_service as ServiceDate,
        procedure_code_9 as ProcedureCode,
         c.short_description as ProcedureDescription
from mobe_live.medical_claims_wide a 
inner join resources.drg b 
	on a.drg = b.MS_DRG 
inner join resources.icd10_proc_2015 c 
	on a.procedure_code_9 = c.procedure_code
where   
    year(a.date_of_service) = 2015
	and cms_place_of_service_id = 21
	and b.TYPE = "SURG"
    and a.member_uniq_id in (select member_uniq_id from mobe_live.eligibility_wide
                                    where control_group = 0 and group_id_prefix in ('BCBSTX', 'BCBSIL'))




union

select  distinct
        a.member_uniq_id as MemberId, 
        a.group_id_1 as InsuranceGroup, 
        a.group_id_2 as InsuranceGroupPlan, 
concat(a.patient_last_name,', ',a.patient_first_name) as PatientName,
        a.service_provider_name as ProviderName,
        a.date_of_service as ServiceDate,
        procedure_code_10 as ProcedureCode,
        c.short_description as ProcedureDescription
from mobe_live.medical_claims_wide a 
inner join resources.drg b 	
	on a.drg = b.MS_DRG 
inner join resources.icd10_proc_2015 c 
	on a.procedure_code_10 = c.procedure_code
where   
    year(a.date_of_service) = 2015
	and cms_place_of_service_id = 21
	and b.TYPE = "SURG"
    and a.member_uniq_id in (select member_uniq_id from mobe_live.eligibility_wide
                                    where control_group = 0 and group_id_prefix in ('BCBSTX', 'BCBSIL'))

