truncate mobe_live.report_dx;

insert into mobe_live.report_dx
select
a.member_uniq_id as MemberId,
concat(b.patient_last_name,', ',b.patient_first_name) as PatientName,
a.icd_code_from_claim as ICD,
a.code_description as ICDDescription,
case 
when a.isPrimary = 1 then 1
when a.isSecondary = 1 then 2
else 3
end as CodeType,
a.service_start_date as ServiceDate
From mobe_live.member_dx_details a
inner join mobe_live.eligibility_wide b 
     on a.member_uniq_id = b.member_uniq_id
where 
     control_group = 0 
     and group_id_prefix in ('BCBSTX', 'BCBSIL')
                      
;


