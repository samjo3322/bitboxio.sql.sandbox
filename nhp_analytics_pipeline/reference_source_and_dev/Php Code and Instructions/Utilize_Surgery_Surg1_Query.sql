delete from mobe_live.report_surg
where ProcedureCode IS Null;

delete from mobe_live.report_surg
where ProcedureDescription is Null;

truncate mobe_live.report_surg_1;

insert into mobe_live.report_surg_1
select distinct
MemberId,
PatientName,
ProviderName,
ServiceDate,
#ProcedureCode,
#ProcedureDescription,
#length(ProcedureDescription) as Len
group_concat(distinct ProcedureDescription order by length(ProcedureDescription) desc separator '; ') 
from mobe_live.report_surg
group by MemberId, PatientName, ProviderName, ServiceDate