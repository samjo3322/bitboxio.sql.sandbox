Updating Php

1. Update report_med

Run Utilize_Med_Query (Inserts ICD 9, then ICD 10)

2. Update report_rx

Run Utilize_RXClaims_Temp_Query

Run Utilize_RXClaims_Query (Can drop temp built for speed)

3. Update report_surg_1

Run Utilize_Member_procedures_details

Run Utilize_Surgery_CPT_Query_from_procedure_table

Run Utilize_DRG_ICD_10 2015-2018 queries

Run Utilize_DRG_ICD_09_PCS

Run Utilize_Surgery_Surg1_Query

Run Utilize_Surg_Pull

4. Update report_dx

Run Utilize_DX_Sorted


