#drop table if exists mobe_live.presales_dx_full;
CREATE TABLE mobe_live.`member_dx_details_2` (
  `member_uniq_id` varchar(255) DEFAULT NULL,
  `icd_code_from_claim` varchar(20) DEFAULT NULL,
  `code_description` text,
  `claim_number` varchar(255) DEFAULT NULL,
  `service_start_date` date DEFAULT NULL,
  `isPrimary`   int(1) DEFAULT NULL,
  `isSecondary` int(1) DEFAULT NULL,
  `isACSC`      int(1) DEFAULT NULL,
  `episode_of_care_description` varchar(255) DEFAULT NULL,
  KEY `member_uniq_id` (`member_uniq_id`),
  KEY `icd` (`icd_code_from_claim`),
  KEY `claim_number` (`claim_number`),
  KEY `date` (`service_start_date`),
  KEY `isPrimary` (`isPrimary`),
  KEY `isSecondary` (`isSecondary`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

truncate table mobe_live.member_dx_details_2;
insert into member_dx_details_2
(member_uniq_id, icd_code_from_claim,claim_number, service_start_date)
(select distinct member_uniq_id,
                icd_1,
                claim_number,
                service_start_date
from mobe_live.medical_claims_wide
where icd_1 is not null and fully_adjudicated = 1)
union
(select distinct member_uniq_id,
                icd_2,
                claim_number,
                service_start_date
from mobe_live.medical_claims_wide
where icd_2 is not null and fully_adjudicated = 1)
union
(select distinct member_uniq_id,
                icd_3,
                claim_number,
                service_start_date
from mobe_live.medical_claims_wide
where icd_3 is not null and fully_adjudicated = 1)
union
(select distinct member_uniq_id,
                icd_4,
                claim_number,
                service_start_date
from mobe_live.medical_claims_wide
where icd_4 is not null and fully_adjudicated = 1)
union
(select distinct member_uniq_id,
                icd_5,
                claim_number,
                service_start_date
from mobe_live.medical_claims_wide
where icd_5 is not null and fully_adjudicated = 1)
union
(select distinct member_uniq_id,
                icd_6,
                claim_number,
                service_start_date
from mobe_live.medical_claims_wide
where icd_6 is not null and fully_adjudicated = 1)
union
(select distinct member_uniq_id,
                icd_7,
                claim_number,
                service_start_date
from mobe_live.medical_claims_wide
where icd_7 is not null and fully_adjudicated = 1)
union
(select distinct member_uniq_id,
                icd_8,
                claim_number,
                service_start_date
from mobe_live.medical_claims_wide
where icd_8 is not null and fully_adjudicated = 1)
union
(select distinct member_uniq_id,
                icd_9,
                claim_number,
                service_start_date
from mobe_live.medical_claims_wide
where icd_9 is not null and fully_adjudicated = 1)
union
(select distinct member_uniq_id,
                icd_10,
                claim_number,
                service_start_date
from mobe_live.medical_claims_wide
where icd_10 is not null and fully_adjudicated = 1);

update mobe_live.member_dx_details_2 a
set isPrimary = 1
where a.icd_code_from_claim in (select icd_10 from resources.uh_dx_icd_final where `Primary` = 1);

update mobe_live.member_dx_details_2 a
set isPrimary = 1
where a.icd_code_from_claim in (select icd_9 from resources.uh_dx_icd_final where `Primary` = 1);

update mobe_live.member_dx_details_2 a
set isACSC = 1
where a.icd_code_from_claim in (select icd_10 from resources.uh_dx_icd_final where is_ACSC = 1);

update mobe_live.member_dx_details_2 a
set isACSC = 1
where a.icd_code_from_claim in (select icd_9 from resources.uh_dx_icd_final where is_ACSC = 1);

update mobe_live.member_dx_details_2 a
set isSecondary = 1
where a.icd_code_from_claim in (select icd_10 from resources.uh_dx_icd_final where catagory = 'Secondary');

update mobe_live.member_dx_details_2 a
set isSecondary = 1
where a.icd_code_from_claim in (select icd_9 from resources.uh_dx_icd_final where catagory = 'Secondary');

update mobe_live.member_dx_details_2 a, resources.uh_dx_icd_final b
set a.episode_of_care_description = b.episode_of_care_description
where (a.icd_code_from_claim = b.icd_10);

update mobe_live.member_dx_details_2 a, resources.uh_dx_icd_final b
set a.episode_of_care_description = b.episode_of_care_description
where (a.icd_code_from_claim = b.icd_9);


update mobe_live.member_dx_details_2 a, resources.uh_dx_icd_final b
set a.code_description = b.code_description
where (a.icd_code_from_claim = b.icd_10);

update mobe_live.member_dx_details_2 a, resources.uh_dx_icd_final b
set a.code_description = b.code_description
where (a.icd_code_from_claim = b.icd_9);