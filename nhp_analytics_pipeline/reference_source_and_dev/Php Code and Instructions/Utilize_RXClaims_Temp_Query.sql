truncate mobe_live.report_rx;

insert into mobe_live.report_rx_temp
SELECT distinct
    a.member_uniq_id as MemberId,
    a.product_type as ProductType,
#ifnull(concat(c.patient_last_name,', ',c.patient_first_name), concat(a.patient_last_name,', ',a.patient_first_name)) as PatientName,
   a.ndc as NDC,
   b.Drug_name as Medication,
   b.dosage_form as DosageForm,
   concat(b.strength, ' ',b.strength_unit_of_measure) as Dosage,
   b.route_of_administration as Route,
   b.maintenance_ind,
   b.generic_ind,
   a.date_filled as FirstDateFilled,
   a.date_filled as LastDateFilled,
   a.refill_number as TotalRefills
 FROM mobe_live.rx_claims_wide a 
 inner join mobe_live.eligibility_wide c 
	on a.member_uniq_id = c.member_uniq_id 
 inner join medispan.NDC_details b  
	on a.ndc = b.NDC
