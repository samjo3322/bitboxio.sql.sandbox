truncate mobe_live.report_surg;

insert into mobe_live.report_surg

select  distinct
        a.member_uniq_id as MemberId,        
concat(c.patient_last_name,', ',c.patient_first_name) as PatientName,
        a.service_provider_name as ProviderName,
        a.service_start_date as ServiceDate,
        a.procedure_code_from_claim as ProcedureCode,
        b.description as  ProcedureDescription
from mobe_live.member_procedure_details a 
inner join resources.cpt_common b 
	on procedure_code_from_claim = Code
inner join mobe_live.eligibility_wide c 
	on a.member_uniq_id = c.member_uniq_id 
		and c.control_group = 0
		and upper(c.group_id_prefix) in ('BCBSTX', 'BCBSIL')

where procedure_code_from_claim between 10021 and 69990
	

