#select * from mobe_live.report_med
truncate mobe_live.report_med;
insert into mobe_live.report_med
SELECT Distinct
a.member_uniq_id as MemberId,
a.group_id_1 as InsuranceGroup,
#a.group_id_2 as InsuranceGroupPlan,
#concat(ifnull(c.patient_last_name,a.patient_last_name),', ',ifnull(c.patient_first_name, a.patient_first_name)) as PatientName,
concat(c.patient_last_name,', ',c.patient_first_name) as PatientName,
a.service_provider_name as ProviderName,
a.service_provider_type as ProviderType,
#a.service_provider_specialty as ProviderSpecialty,
date_format(c.patient_dob, "%Y-%m-%d") as DOB,
timestampdiff(Year,c.patient_dob,curdate()) as Age,
c.member_gender as PatientGender,
a.date_of_service as ServiceDate,
#datediff(a.service_end_date, a.service_start_date) as LOS,
datediff(a.service_end_date, a.service_start_date) + 1 as LOS,  
a.icd_1 as ICDCode,
b.code_description as ICDDescription
From mobe_live.medical_claims_wide a
   inner join resources.uh_dx_icd_all b 
      on a.icd_1 = b.icd_9
   inner join mobe_live.eligibility_wide c 
      on a.member_uniq_id = c.member_uniq_id 
where a.icd_version = 9
and upper(c.group_id_prefix) in ('BCBSTX', 'BCBSIL')
and c.control_group = 0

;

#########

insert into mobe_live.report_med
SELECT Distinct
a.member_uniq_id as MemberId,
a.group_id_1 as InsuranceGroup,
#a.group_id_2 as InsuranceGroupPlan,
#concat(ifnull(c.patient_last_name,a.patient_last_name),', ',ifnull(c.patient_first_name, a.patient_first_name)) as PatientName,
concat(c.patient_last_name,', ',c.patient_first_name) as PatientName,
a.service_provider_name as ProviderName,
a.service_provider_type as ProviderType,
#a.service_provider_specialty as ProviderSpecialty,
date_format(c.patient_dob, "%Y-%m-%d") as DOB,
timestampdiff(Year,c.patient_dob,curdate()) as Age,
c.member_gender as PatientGender,
a.date_of_service as ServiceDate,
#datediff(a.service_end_date, a.service_start_date) as LOS,
datediff(a.service_end_date, a.service_start_date) + 1 as LOS,  
a.icd_1 as ICDCode,
b.code_description as ICDDescription
From mobe_live.medical_claims_wide a
   inner join resources.uh_dx_icd_all b 
      on a.icd_1 = b.icd_10
   inner join mobe_live.eligibility_wide c 
      on a.member_uniq_id = c.member_uniq_id 
where a.icd_version = 10
      and upper(c.group_id_prefix) in ('BCBSTX', 'BCBSIL')
      and c.control_group = 0;