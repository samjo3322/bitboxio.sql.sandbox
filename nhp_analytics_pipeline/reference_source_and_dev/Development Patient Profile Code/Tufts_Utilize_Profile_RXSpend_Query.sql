SELECT 
    c.Drug_name as Medication,
    count(distinct(a.member_uniq_id)) as MemberCount,
#    count(distinct(a.subscriber_id)) as SubscriberCount,
    count(distinct(a.claim_number)) as ClaimCount,
    count(distinct(a.prescribing_provider_npi)) as ProviderNPICount,
    sum(a.amt_paid) as TotalPaidAmount,
    sum(a.amt_paid) / count(distinct(a.member_uniq_id)) as AmountPaidPerMember,
    sum(a.amt_allowed) as TotalAllowedAmount,
    sum(a.amt_allowed) / count(distinct(a.member_uniq_id)) as AmountAllowedPerMember
 FROM tufts_presales.rx_claims a 
  join tufts_presales.eligibility_wide b 
        on a.member_uniq_id = b.member_uniq_id
			and a.date_prescribed
			between b.eligibility_start_date 
				and b.eligibility_end_date
  left join medispan.NDC_details c 
        on a.ndc = c.NDC

where a.fully_adjudicated = 1 and a.exclude_from_calcs = 0
group by c.Drug_name
