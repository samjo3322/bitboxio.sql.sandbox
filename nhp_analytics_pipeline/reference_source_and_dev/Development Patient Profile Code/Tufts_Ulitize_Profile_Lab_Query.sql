select 
group_id_1,
group_id_2,
a.product_type,
a.funding_type,
a.date_of_service,
a.service_start_date,
a.service_end_date,
a.cms_place_of_service_id,
a.procedure_code,
a.procedure_modifier,
a.procedure_code_2,
a.procedure_modifier_2,
a.icd_1,
a.icd_2,
a.icd_3,
a.rev_code,
a.drg,
count(distinct(a.member_uniq_id)) as MemberCount,
count(distinct(a.subscriber_id)) as SubscriberCount,
count(distinct(a.claim_number)) as ClaimCount,
count(distinct(a.ioc)) as IOCCount,
count(distinct(a.service_provider_npi)) as ProviderNPICount,
sum(a.amt_paid) as TotalPaidAmount,
sum(a.amt_paid) / count(distinct(a.member_uniq_id)) as AmountPaidPerMember,
sum(a.amt_allowed) as TotalAllowedAmount,
sum(a.amt_allowed) / count(distinct(a.member_uniq_id)) as AmountAllowedPerMember
from tufts_presales.medical_claims_wide a 
inner join tufts_presales.eligibility_wide b 
        on a.member_uniq_id = b.member_uniq_id 
        and a.service_start_date 
			between b.eligibility_start_date 
				and b.eligibility_end_date
Where a.fully_adjudicated = 1 and a.exclude_from_calcs = 0
  and (
        a.rev_code in (300, 301, 302, 303, 304, 305, 307, 309, 310, 311, 312, 314, 319)
        or cast(a.procedure_code as signed) between 80047 and 89398
      )
group by group_id_1, group_id_2, a.product_type, a.funding_type, a.date_of_service,
a.service_start_date, a.service_end_date, a.cms_place_of_service_id, a.procedure_code,
a.procedure_modifier, a.procedure_code_2, a.procedure_modifier_2, a.icd_1, a.icd_2, a.icd_3,
a.rev_code
