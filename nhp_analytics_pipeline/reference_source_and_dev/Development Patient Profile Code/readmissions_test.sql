DELIMITER $$
drop procedure if exists mobe_live.claims_per_timeframe_test;
CREATE DEFINER=`utilize_health_sandbox`@`%` PROCEDURE mobe_live.claims_per_timeframe_test(i_window int, -- number of days that create a violation window
                                       i_target int, -- number of violations required to trigger identification
                                       i_rule_name varchar(255))
                                       
BEGIN                                      
	DECLARE v_row_count 			int default 0;
    DECLARE v_date_1 				date default null;
    DECLARE v_date_2 				date default null;
    DECLARE v_date_3 				date default null;
    DECLARE v_date_4 				date default null;
	DECLARE v_member_1 				varchar(255) default null;
    DECLARE v_member_2 				varchar(255) default null;
    DECLARE done 					int;
    DECLARE v_target_count 			int default 0;
    DECLARE v_days 					int default 0;
    DECLARE v_hold_claim_numbers 	longtext;
    DECLARE v_claim_number 			varchar(255) default null;
    DECLARE v_created_at 			datetime;
    DECLARE v_concat_ind 			int default 0;
                                       
    DECLARE claim_date CURSOR FOR
    select distinct member_uniq_id,
                    claim_number,
                    service_start_date,
                    service_end_date
    FROM mobe_live.readmission_test
    order by member_uniq_id,service_start_date,service_end_date,claim_number asc;
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
    
    OPEN claim_date;
		 claim_loop: LOOP
			IF done = 1 THEN
				CLOSE claim_date;
				LEAVE claim_loop;
            END IF;
            
    SET v_created_at = sysdate();        
            
    FETCH claim_date INTO v_member_1,v_claim_number,v_date_1,v_date_3;
    
    SET v_row_count = v_row_count + 1;        
                                       
    


    
END LOOP claim_loop;       
                                       
END$$                                      
DELIMITER ;    