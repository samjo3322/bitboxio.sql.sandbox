###create table to house members with claims data for primary condition in claims###
   CREATE TABLE `member_presales_acsc_claims` (
  `member_uniq_id` varchar(255) DEFAULT NULL,
  `claim_number` varchar(55) DEFAULT NULL,
  `claim_line_number` int(3) DEFAULT NULL,
  `ioc` varchar(255) DEFAULT NULL,
  `date_of_service` date DEFAULT NULL,
  `service_start_date` date DEFAULT NULL,
  `service_end_date` date DEFAULT NULL,
  `service_provider_npi` varchar(10) DEFAULT NULL,
  `service_provider_specialty` varchar(255) DEFAULT NULL,
  `cms_place_of_service_id` varchar(3) DEFAULT NULL,
  `amt_billed` decimal(14,2) DEFAULT NULL,
  `amt_allowed` decimal(14,2) DEFAULT NULL,
  `amt_paid` decimal(14,2) DEFAULT NULL,
  `amt_copay` decimal(14,2) DEFAULT NULL,
  `amt_coinsurance` decimal(14,2) DEFAULT NULL,
  `amt_deductible` decimal(14,2) DEFAULT NULL,
  `amt_cob` decimal(14,2) DEFAULT NULL,
  `amt_patient_responsibility` decimal(14,2) DEFAULT NULL,
  `bundled_payment_ind` int(1) DEFAULT NULL,
  `admit_ind` varchar(1) DEFAULT NULL,
  `icd_version` int(2) DEFAULT NULL,
  KEY `claim_number` (`claim_number`),
  KEY `member_uniq_id` (`member_uniq_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


###Insert Data into member_presales_secondary_claims contains members who have claims containing a primary condition###
insert into tufts_presales.member_presales_acsc_claims 
  
  select
  mcw.member_uniq_id as member_uniq_id,
  claim_number,
  claim_line_number,
  ioc,
  date_of_service,
  service_start_date,
  service_end_date,
  service_provider_npi,
  service_provider_specialty,
  cms_place_of_service_id,
  amt_billed,
  amt_allowed,
  amt_paid,
  amt_copay,
  amt_coinsurance,
  amt_deductible,
  amt_cob,
  amt_patient_responsibility,
  bundled_payment_ind,
  admit_ind,
  icd_version
from tufts_presales.medical_claims_wide mcw
	inner join tufts_presales.eligibility_wide ew
		on mcw.member_uniq_id = ew.member_uniq_id
			and service_start_date 
				between eligibility_start_date 
					and eligibility_end_date
where fully_adjudicated = 1 and exclude_from_calcs = 0
and mcw.member_uniq_id  in (select distinct member_uniq_id from tufts_presales.member_dx_details_long
							where isACSC = 1 and isPrimary is null)
;
  
###create table to house claims data for secondary conditions with icd codes###
   CREATE TABLE `member_presales_acsc_claims_long` (
  `member_uniq_id` varchar(255) DEFAULT NULL,
  `claim_number` varchar(55) DEFAULT NULL,
  `claim_line_number` int(3) DEFAULT NULL,
  `ioc` varchar(255) DEFAULT NULL,
  `date_of_service` date DEFAULT NULL,
  `service_start_date` date DEFAULT NULL,
  `service_end_date` date DEFAULT NULL,
  `service_provider_npi` varchar(10) DEFAULT NULL,
  `service_provider_specialty` varchar(255) DEFAULT NULL,
  `cms_place_of_service_id` varchar(3) DEFAULT NULL,
  `amt_billed` decimal(14,2) DEFAULT NULL,
  `amt_allowed` decimal(14,2) DEFAULT NULL,
  `amt_paid` decimal(14,2) DEFAULT NULL,
  `amt_copay` decimal(14,2) DEFAULT NULL,
  `amt_coinsurance` decimal(14,2) DEFAULT NULL,
  `amt_deductible` decimal(14,2) DEFAULT NULL,
  `amt_cob` decimal(14,2) DEFAULT NULL,
  `amt_patient_responsibility` decimal(14,2) DEFAULT NULL,
  `bundled_payment_ind` int(1) DEFAULT NULL,
  `admit_ind` varchar(1) DEFAULT NULL,
  `icd_version` int(2) DEFAULT NULL,
  `icd_code_from_claim` varchar(20) DEFAULT NULL,
  `code_description` text DEFAULT NULL,
  KEY `claim_number` (`claim_number`),
  KEY `member_uniq_id` (`member_uniq_id`),
  KEY `icd_code_from_claim` (`icd_code_from_claim`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

###Join primary claims to primary icd. This will create a cartesian product if a claim has multiple primary icds###
insert into tufts_presales.member_presales_acsc_claims_long

select
  mpp.member_uniq_id as member_uniq_id,
  mpp.claim_number,
  claim_line_number,
  ioc,
  date_of_service,
  mpp.service_start_date,
  service_end_date,
  service_provider_npi,
  service_provider_specialty,
  cms_place_of_service_id,
  amt_billed,
  amt_allowed,
  amt_paid,
  amt_copay,
  amt_coinsurance,
  amt_deductible,
  amt_cob,
  amt_patient_responsibility,
  bundled_payment_ind,
  admit_ind,
  icd_version,
  icd_code_from_claim,
  code_description
from tufts_presales.member_presales_acsc_claims  mpp
	inner join tufts_presales.member_dx_details_long mdl
		on mpp.member_uniq_id = mdl.member_uniq_id
			and mpp.claim_number = mdl.claim_number
				and isACSC = 1 and isPrimary is null
;