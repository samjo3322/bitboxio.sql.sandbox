select
count(distinct(a.member_uniq_id)) as MemberCount,
count(distinct(a.subscriber_id)) as SubscriberCount,
count(distinct(a.claim_number)) as ClaimCount,
count(distinct(a.ioc)) as IOCCount,
count(distinct(a.service_provider_npi)) as ProviderNPICount,
sum(a.amt_paid) as TotalPaidAmount,
sum(a.amt_paid) / count(distinct(a.member_uniq_id)) as AmountPaidPerMember,
sum(a.amt_allowed) as TotalAllowedAmount,
sum(a.amt_allowed) / count(distinct(a.member_uniq_id)) as AmountAllowedPerMember
From mobe_live.medical_claims_wide a 
  join mobe_live.member_dx_details_long b 
       on a.member_uniq_id = b.member_uniq_id 
          and a.claim_number = b.claim_number  
inner join mobe_live.eligibility_wide c 
       on b.member_uniq_id = c.member_uniq_id
where fully_adjudicated = 1
       and icd_code_from_claim in (select distinct trim(code)
                                     from resources.icd10_2017
                                          where description like '%Opioid abuse%'
												or  description like '%Opioid dependence%'
												or description like '%Sedative, hypnotic or anxiolytic dependence%'
												or  description like '%Sedative, hypnotic or anxiolytic abuse%');             

