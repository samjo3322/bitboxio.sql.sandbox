SELECT distinct
b.icd_code_from_claim,
a.member_uniq_id,
a.lob,
a.product_type,
a.funding_type,
a.subscriber_id,
a.claim_number,
a.claim_line_number,
a.date_of_service,
a.service_start_date,
a.service_end_date,
a.cms_place_of_service_id,
a.service_provider_type,
a.admit_ind,
a.ioc,
a.service_provider_npi,
a.amt_paid as TotalAmtPaid,
a.amt_allowed as TotalAmtAllowed
From tufts_presales.medical_claims_wide a 
inner join tufts_presales.member_dx_details_long b 
      on a.member_uniq_id = b.member_uniq_id 
         and a.claim_number = b.claim_number 
inner join tufts_presales.eligibility_wide c 
      on a.member_uniq_id = c.member_uniq_id 
where fully_adjudicated = 1 and exclude_from_calcs = 0
		 and b.isACSC = 1
            and b.isPrimary is null
                 and a.member_uniq_id = 'N1100145993'
                 and a.claim_number = 'U18123E01WP'

#group by 
#	b.icd_code_from_claim,
#	a.member_uniq_id,
#	a.lob,
#	a.product_type,
#	a.funding_type,
#	a.subscriber_id,
#	a.claim_number,
#    a.claim_line_number,
#	a.date_of_service,
#	a.service_start_date,
#	a.service_end_date,
#	a.cms_place_of_service_id,
#	a.service_provider_type,
#	a.admit_ind,
#	a.ioc,
#	a.service_provider_npi
order by a.date_of_service desc     
