select 
a.service_provider_specialty,
count(distinct(a.member_uniq_id)) as MemberCount,
count(distinct(a.subscriber_id)) as SubscriberCount,
count(distinct(a.claim_number)) as ClaimCount,
count(distinct(a.ioc)) as IOCCount,
count(distinct(a.service_provider_npi)) as ProviderNPICount,
sum(a.amt_paid) as TotalPaidAmount,
sum(a.amt_paid) / count(distinct(a.member_uniq_id)) as AmountPaidPerMember,
sum(a.amt_allowed) as TotalAllowedAmount,
sum(a.amt_allowed) / count(distinct(a.member_uniq_id)) as AmountAllowedPerMember
from tufts_presales.medical_claims_wide a 
inner join tufts_presales.eligibility_wide b 
	       on a.member_uniq_id = b.member_uniq_id
Where a.fully_adjudicated = 1 and a.exclude_from_calcs = 0
  and   (
          (a.service_provider_specialty like '%Physical Therapist%'           or
           a.service_provider_specialty like '%Physical Therapy%'             or
           a.service_provider_specialty like '%Physical Therapy Assistant%'   or
           a.service_provider_specialty like '%Occupational Therapist%'       or
           a.service_provider_specialty like '%Occupational Therapy%')
        or cast(a.procedure_code as SIGNED) between 97001 and 97755 
        )

group by a.service_provider_specialty
