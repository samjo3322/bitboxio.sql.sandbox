select 
a.cms_place_of_service_id,
count(distinct(a.member_uniq_id)) as MemberCount,
count(distinct(a.subscriber_id)) as SubscriberCount,
count(distinct(a.claim_number)) as ClaimCount,
count(distinct(a.ioc)) as IOCCount,
count(distinct(a.service_provider_npi)) as ProviderNPICount,
sum(a.amt_paid) as TotalPaidAmount,
sum(a.amt_paid) / count(distinct(a.member_uniq_id)) as AmountPaidPerMember,
sum(a.amt_allowed) as TotalAllowedAmount,
sum(a.amt_allowed) / count(distinct(a.member_uniq_id)) as AmountAllowedPerMember
from tufts_presales.medical_claims_wide a 
inner join tufts_presales.eligibility_wide b 
      on a.member_uniq_id = b.member_uniq_id 
		and service_start_date 
			between eligibility_start_date 
				and eligibility_end_date
					and a.fully_adjudicated = 1 and a.exclude_from_calcs = 0
group by a.cms_place_of_service_id
