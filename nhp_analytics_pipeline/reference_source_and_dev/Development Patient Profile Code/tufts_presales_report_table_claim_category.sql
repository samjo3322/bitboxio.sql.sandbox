CREATE TABLE `member_presales_report_table_category` (
  `member_uniq_id` varchar(255) DEFAULT NULL,
  `claim_number` varchar(255) DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  `isPrimary` int(1) DEFAULT NULL,
  `isSecondary` int(1) DEFAULT NULL,
  `isACSC` int(1) DEFAULT NULL,
  `chronicity` varchar(50) DEFAULT NULL,
  KEY `member_uniq_id` (`member_uniq_id`),
  KEY `claim_number` (`claim_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into tufts_presales.member_presales_report_table_category

select distinct
member_uniq_id,
claim_number,
category,
isPrimary,
isSecondary,
isACSC,
chronicity
from tufts_presales.member_dx_details_long
;            