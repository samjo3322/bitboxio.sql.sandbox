CREATE TABLE `member_presales_report_summary_table` (
  `member_uniq_id` varchar(255) DEFAULT NULL,
  `claim_number` varchar(255) DEFAULT NULL,
  `lob` varchar(55) DEFAULT NULL,
  `date_of_service` date DEFAULT NULL,
  `amt_allowed` decimal(14,2) DEFAULT NULL,
  `amt_paid` decimal(14,2) DEFAULT NULL,
  `amt_cob` decimal(14,2) DEFAULT NULL,
  `amt_patient_responsibility` decimal(14,2) DEFAULT NULL,
  KEY `member_uniq_id` (`member_uniq_id`),
  KEY `claim_number` (`claim_number`),
  KEY `date_of_service` (`date_of_service`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into tufts_presales.member_presales_report_summary_table

select
a.member_uniq_id,
a.claim_number,
lob,
a.date_of_service,
sum(amt_paid) as AmtPaid,
sum(amt_allowed) as AmtAllowed,
sum(amt_cob) as AmtCob,
sum(amt_patient_responsibility)
from tufts_presales.member_presales_claims a 
group by 
			a.member_uniq_id, 
			a.claim_number, 
            lob
            
;            