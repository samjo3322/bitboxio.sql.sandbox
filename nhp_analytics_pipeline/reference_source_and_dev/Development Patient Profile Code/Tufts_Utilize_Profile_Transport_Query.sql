select 
count(distinct(a.member_uniq_id)) as MemberCount,
count(distinct(a.subscriber_id)) as SubscriberCount,
count(distinct(a.claim_number)) as ClaimCount,
count(distinct(a.ioc)) as IOCCount,
count(distinct(a.service_provider_npi)) as ProviderNPICount,
sum(a.amt_paid) as TotalPaidAmount,
sum(a.amt_paid) / count(distinct(a.member_uniq_id)) as AmountPaidPerMember,
sum(a.amt_allowed) as TotalAllowedAmount,
sum(a.amt_allowed) / count(distinct(a.member_uniq_id)) as AmountAllowedPerMember
from tufts_presales.medical_claims_wide a 
inner join tufts_presales.eligibility_wide b 
	on a.member_uniq_id = b.member_uniq_id
		and service_start_date 
			between eligibility_start_date 
				and eligibility_end_date
where a.fully_adjudicated = 1 and a.exclude_from_calcs = 0
           and  (
   
         (a.service_provider_specialty like '%Ambulance%' or
          a.service_provider_specialty like '%Transportation%')
	      or procedure_code in ('A0021', 'A0080', 'A0090', 'A0100', 'A0110', 'A0120', 'A0130', 'A0140', 'A0160', 
                                'A0170', 'A0180', 'A0190', 'A0200', 'A0210', 'A0225', 'A0384', 'A0392', 'A0420', 
                                'A0422', 'A0424', 'A0426', 'A0427', 'A0428', 'A0429', 'A0430', 'A0431', 'A0432',
                                'A0434', 'A0888', 'A0998', 'A0999')
				)           
    