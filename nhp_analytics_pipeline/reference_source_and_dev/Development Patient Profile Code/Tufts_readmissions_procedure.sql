DELIMITER $$
CREATE PROCEDURE `claims_per_timeframe`(i_window int, -- number of days that create a violation window
                                       i_target int, -- number of violations required to trigger identification
                                       i_rule_name varchar(255))
BEGIN
	
  declare v_row_count int default 0;
  
  DECLARE v_date_1 date default null;
  declare v_date_2 date default null;
  declare v_member_1 varchar(255) default null;
  declare v_member_2 varchar(255) default null;
  declare done int;
  declare v_target_count int default 0;
  declare v_days int default 0;
  declare v_hold_claim_numbers longtext;
  declare v_claim_number varchar(255) default null;
  declare v_created_at datetime;
  declare v_concat_ind int default 0;
 
  DECLARE claim_date CURSOR FOR
  SELECT distinct member_uniq_id,
                  claim_number,
                  service_start_date
  From tufts_presales.medical_claims_wide a 
inner join tufts_presales.eligibility_wide b
       on b.member_uniq_id = b.member_uniq_id
         and a.date_of_service 
			between b.eligibility_start_date 
				and b.eligibility_end_date
where a.fully_adjudicated = 1 
	and a.exclude_from_calcs = 0
		and a.admit_ind = 'Y'

  order by member_uniq_id,service_start_date,claim_number asc;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
open claim_date;
  claim_loop: loop
    if done = 1 then
      close claim_date;
      leave claim_loop;
    end if;  
      
set v_created_at = sysdate();

  fetch claim_date into v_member_1,v_claim_number,v_date_1;
  set v_row_count = v_row_count + 1;
    
#    if v_row_count = 1 then
#      set v_date_2 = v_date_1;
#      set v_member_2 = v_member_1;
#      set v_target_count = 1;
#        if v_target_count > i_target then
#          set v_hold_claim_numbers = v_claim_number;
#        end if;
#    else
      if v_member_1 = v_member_2 then
        set v_days = 0;
        set v_days = datediff(v_date_1,v_date_2);
          if v_days < i_window and v_days != 0 then 
            set v_target_count = v_target_count + 1;
            set v_concat_ind = 1;
 #           select v_target_count, v_days,v_date_1, v_date_2;
          end if;
          if v_target_count > i_target and v_concat_ind = 1 then
            set v_hold_claim_numbers = concat(v_hold_claim_numbers,'|',v_claim_number);
          end if;
        set v_date_2 = v_date_1;
        set v_member_2 = v_member_1;
        set v_concat_ind = 0;
      else
        if v_target_count > i_target then
          insert into tufts_presales.member_presales_readmissions
          (member_uniq_id, rule_name, claim_string, trigger_count, created_at)
          values
          (v_member_2,i_rule_name,v_hold_claim_numbers,v_target_count,v_created_at);
        end if;
        set v_hold_claim_numbers = v_claim_number;
        set v_concat_ind = 0;
        set v_days = 0;
        set v_date_2 = v_date_1;
        set v_member_2 = v_member_1;
        set v_target_count = 1;
      end if;
 #   end if;
end loop claim_loop;

END$$
DELIMITER ;

