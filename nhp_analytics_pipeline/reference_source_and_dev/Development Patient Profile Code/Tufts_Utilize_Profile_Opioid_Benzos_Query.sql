select
count(distinct(a.member_uniq_id)) as MemberCount,
count(distinct(a.subscriber_id)) as SubscriberCount,
count(distinct(a.claim_number)) as ClaimCount,
count(distinct(a.ioc)) as IOCCount,
count(distinct(a.service_provider_npi)) as ProviderNPICount,
sum(a.amt_paid) as TotalPaidAmount,
sum(a.amt_paid) / count(distinct(a.member_uniq_id)) as AmountPaidPerMember,
sum(a.amt_allowed) as TotalAllowedAmount,
sum(a.amt_allowed) / count(distinct(a.member_uniq_id)) as AmountAllowedPerMember
From tufts_presales.medical_claims_wide a 
inner join tufts_presales.eligibility_wide b
       on b.member_uniq_id = b.member_uniq_id
         and a.service_start_date 
			between b.eligibility_start_date 
				and b.eligibility_end_date
where a.fully_adjudicated = 1 and a.exclude_from_calcs = 0
       and a.claim_number in (select claim_number 
                                     from tufts_presales.member_dx_details_long a
										inner join resources.icd10_2017 b
											on a.icd_code_from_claim = b.code
                                          where b.description like '%Opioid abuse%'
												or  b.description like '%Opioid dependence%'
												or  b.description like '%Sedative, hypnotic or anxiolytic dependence%'
												or  b.description like '%Sedative, hypnotic or anxiolytic abuse%');             

