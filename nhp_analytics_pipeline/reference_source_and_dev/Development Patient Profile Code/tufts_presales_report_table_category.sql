CREATE TABLE `member_presales_report_table` (
  `member_uniq_id` varchar(255) DEFAULT NULL,
  `claim_number` varchar(255) DEFAULT NULL,
  `lob` varchar(55) DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  `date_of_service` date DEFAULT NULL,
  `isPrimary` int(1) DEFAULT NULL,
  `isSecondary` int(1) DEFAULT NULL,
  `isACSC1` int(1) DEFAULT NULL,
  `amt_allowed` decimal(14,2) DEFAULT NULL,
  `amt_paid` decimal(14,2) DEFAULT NULL,
  `amt_cob` decimal(14,2) DEFAULT NULL,
  `amt_patient_responsibility` decimal(14,2) DEFAULT NULL,
  KEY `member_uniq_id` (`member_uniq_id`),
  KEY `claim_number` (`claim_number`),
  KEY `date_of_service` (`date_of_service`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into tufts_presales.member_presales_report_table

select distinct
a.member_uniq_id,
a.claim_number,
lob,
b.category,
a.date_of_service,
b.isPrimary,
b.isSecondary,
b.isACSC,
sum(amt_paid) as AmtPaid,
sum(amt_allowed) as AmtAllowed,
sum(amt_cob) as AmtCob,
sum(amt_patient_responsibility)
from tufts_presales.member_presales_claims a 
	left join tufts_presales.member_dx_details_long b
		on a.member_uniq_id = b.member_uniq_id
			and a.claim_number = b.claim_number
					and a.date_of_service = b.date_of_service
group by 
			a.member_uniq_id, 
			a.claim_number, 
            b.category, 
            lob,
            a.date_of_service,
            b.isPrimary,
            b.isSecondary,
            b.isACSC
;            