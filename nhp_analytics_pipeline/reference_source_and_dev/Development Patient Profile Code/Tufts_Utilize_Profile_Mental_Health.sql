select distinct
a.member_uniq_id,
a.cms_place_of_service_id,
b.icd_code_from_claim,
a.procedure_code,
a.procedure_code_2,
a.procedure_code_3,
a.procedure_code_4,
a.procedure_code_5,
a.procedure_code_6,
a.procedure_code_7,
a.procedure_code_8,
a.procedure_code_9,
a.procedure_code_10,
count(distinct(a.member_uniq_id)) as MemberCount,
count(distinct(a.subscriber_id)) as SubscriberCount,
count(distinct(a.claim_number)) as ClaimCount,
count(distinct(a.ioc)) as IOCCount,
count(distinct(a.service_provider_npi)) as ProviderNPICount,
sum(a.amt_paid) as TotalPaidAmount,
sum(a.amt_paid) / count(distinct(a.member_uniq_id)) as AmountPaidPerMember,
sum(a.amt_allowed) as TotalAllowedAmount,
sum(a.amt_allowed) / count(distinct(a.member_uniq_id)) as AmountAllowedPerMember
From tufts_presales.medical_claims_wide a 
  join tufts_presales.member_dx_details_long b 
       on a.member_uniq_id = b.member_uniq_id 
          and a.claim_number = b.claim_number  
inner join tufts_presales.eligibility_wide c 
       on b.member_uniq_id = c.member_uniq_id
where a.fully_adjudicated = 1 and a.exclude_from_calcs = 0 
#       and left(icd_code_from_claim, 3) not in ('291', '292') ###This removes the behavioral health icd 9 codes for addiction
                and (icd_code_from_claim in (select trim(code)
                                             from resources.icd10_2017
                                                where code like 'F%'
											       and code not like 'F1%')
            
               or a.cms_place_of_service_id in ('51', '52', '53', '56')
                 or a.icd_1 in ('90791', '90792')
			        or cast(a.procedure_code as Signed) between 90832 and 90899
			           or a.procedure_code in ('H0031', 'H0032', 'H0035', 'H0036', 'H0037', 'H0046', 
                                               'H2012', 'H2013', 'H2030', 'H2031', 'H2032')
		                   or a.procedure_code_2 like 'GZ%'
						      or a.procedure_code_3 like 'GZ%'
                                 or a.procedure_code_4 like 'GZ%'
                                    or a.procedure_code_5 like 'GZ%'
                                       or a.procedure_code_6 like 'GZ%'
                                          or a.procedure_code_7 like 'GZ%'
                                             or a.procedure_code_8 like 'GZ%'
                                                or a.procedure_code_9 like 'GZ%'
                                                   or a.procedure_code_10 like 'GZ%'
			)

group by a.member_uniq_id, a.cms_place_of_service_id, b.icd_code_from_claim, a.procedure_code,
a.procedure_code_2, a.procedure_code_3, a.procedure_code_4, a.procedure_code_5, a.procedure_code_6,
a.procedure_code_7, a.procedure_code_8, a.procedure_code_9, a.procedure_code_10