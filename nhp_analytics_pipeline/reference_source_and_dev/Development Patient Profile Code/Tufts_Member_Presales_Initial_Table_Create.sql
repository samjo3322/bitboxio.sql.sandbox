   CREATE TABLE `member_presales_primary_claims` (
  `member_uniq_id` varchar(255) DEFAULT NULL,
  `claim_number` varchar(55) DEFAULT NULL,
  `claim_line_number` int(3) DEFAULT NULL,
  `ioc` varchar(255) DEFAULT NULL,
  `date_of_service` date DEFAULT NULL,
  `service_start_date` date DEFAULT NULL,
  `service_end_date` date DEFAULT NULL,
  `service_provider_npi` varchar(10) DEFAULT NULL,
  `service_provider_specialty` varchar(255) DEFAULT NULL,
  `cms_place_of_service_id` varchar(3) DEFAULT NULL,
  `amt_billed` decimal(14,2) DEFAULT NULL,
  `amt_allowed` decimal(14,2) DEFAULT NULL,
  `amt_paid` decimal(14,2) DEFAULT NULL,
  `amt_copay` decimal(14,2) DEFAULT NULL,
  `amt_coinsurance` decimal(14,2) DEFAULT NULL,
  `amt_deductible` decimal(14,2) DEFAULT NULL,
  `amt_cob` decimal(14,2) DEFAULT NULL,
  `amt_patient_responsibility` decimal(14,2) DEFAULT NULL,
  `bundled_payment_ind` int(1) DEFAULT NULL,
  `admit_ind` varchar(1) DEFAULT NULL,
  `icd_version` int(2) DEFAULT NULL,
  KEY `claim_number` (`claim_number`),
  KEY `member_uniq_id` (`member_uniq_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;