SELECT 
b.icd_code_from_claim as ICDCode,
'Inpatient Substance Abuse Rehab Visit' as SubstanceAbuseType,
count(distinct(a.member_uniq_id)) as MemberCount,
count(distinct(a.subscriber_id)) as SubscriberCount,
count(distinct(a.claim_number)) as ClaimCount,
count(distinct(a.ioc)) as IOCCount,
count(distinct(a.service_provider_npi)) as ProviderNPICount,
sum(a.amt_Paid) as TotalPaidAmount,
sum(a.amt_Paid) / count(distinct(a.member_uniq_id)) as AmountPaidPerMember,
sum(a.amt_allowed) as TotalAllowedAmount,
sum(a.amt_allowed) / count(distinct(a.member_uniq_id)) as AmountAllowedPerMember
From tufts_presales.medical_claims_wide a 
  inner join tufts_presales.member_dx_details_long b 
          on a.member_uniq_id = b.member_uniq_id 
             and a.claim_number = b.claim_number 
  inner join tufts_presales.eligibility_wide c 
          on a.member_uniq_id = c.member_uniq_id 
where a.fully_adjudicated = 1 and a.exclude_from_calcs = 0
	      and a.cms_place_of_service_id = '55'
group by icd_code_from_claim

union

SELECT 
b.icd_code_from_claim as ICDCode,
'Alcohol dependency DX' as SubstanceAbuseType,
count(distinct(a.member_uniq_id)) as MemberCount,
count(distinct(a.subscriber_id)) as SubscriberCount,
count(distinct(a.claim_number)) as ClaimCount,
count(distinct(a.ioc)) as IOCCount,
count(distinct(a.service_provider_npi)) as ProviderNPICount,
sum(a.amt_Paid) as TotalPaidAmount,
sum(a.amt_Paid) / count(distinct(a.member_uniq_id)) as AmountPaidPerMember,
sum(a.amt_allowed) as TotalAllowedAmount,
sum(a.amt_allowed) / count(distinct(a.member_uniq_id)) as AmountAllowedPerMember
From tufts_presales.medical_claims_wide a 
  inner join tufts_presales.member_dx_details_long b 
    on a.member_uniq_id = b.member_uniq_id 
      and a.claim_number = b.claim_number 
  inner join tufts_presales.eligibility_wide c on a.member_uniq_id = c.member_uniq_id 
where a.fully_adjudicated = 1 and a.exclude_from_calcs = 0
	  and   (  icd_code_from_claim in (select trim(icd_10) 
                              from resources.uh_dx_icd_all
                              where code_description like '%alcohol%'
                                and code_description not like '%toxic effect%'
                                and code_description not like '%maternal care for%'
                                and code_description not like '%nonalcohol%'
                                and code_description not like '%finding%'
                                and code_description not like '%presence of alcohol in blood%'
                                and code_description not like '%blood alcohol level of%'
                                and code_description not like '%encounter%'
                                and code_description not like '%Family%'
                                and icd_10 not in ('E244','F10181','F10182','F1021','G621','G721','I426','K709','P043','Q860'))
            or icd_code_from_claim in (select trim(icd_9) 
                              from resources.uh_dx_icd_all
                              where code_description like '%alcohol%'
                                and code_description not like '%toxic effect%'
                                and code_description not like '%maternal care for%'
                                and code_description not like '%nonalcohol%'
                                and code_description not like '%finding%'
                                and code_description not like '%presence of alcohol in blood%'
                                and code_description not like '%blood alcohol level of%'
                                and code_description not like '%encounter%'
                                and code_description not like '%Family%'
                                and icd_10 not in ('E244','F10181','F10182','F1021','G621','G721','I426','K709','P043','Q860'))
			)
            
group by icd_code_from_claim

union

SELECT 
b.icd_code_from_claim as ICDCode,
'Cocaine dependency DX' as SubstanceAbuseType,
count(distinct(a.member_uniq_id)) as MemberCount,
count(distinct(a.subscriber_id)) as SubscriberCount,
count(distinct(a.claim_number)) as ClaimCount,
count(distinct(a.ioc)) as IOCCount,
count(distinct(a.service_provider_npi)) as ProviderNPICount,
sum(a.amt_Paid) as TotalPaidAmount,
sum(a.amt_Paid) / count(distinct(a.member_uniq_id)) as AmountPaidPerMember,
sum(a.amt_allowed) as TotalAllowedAmount,
sum(a.amt_allowed) / count(distinct(a.member_uniq_id)) as AmountAllowedPerMember
From tufts_presales.medical_claims_wide a 
  inner join tufts_presales.member_dx_details_long b 
    on a.member_uniq_id = b.member_uniq_id 
      and a.claim_number = b.claim_number 
  inner join tufts_presales.eligibility_wide c on a.member_uniq_id = c.member_uniq_id 
where a.fully_adjudicated = 1 and a.exclude_from_calcs = 0
      and (icd_code_from_claim in (select trim(icd_10) 
                                   from resources.uh_dx_icd_all
                                        where code_description like '%cocaine%'
                                              and code_description not like '%assault%'
                                              and code_description not like '%underdosing%')
                                              
		or icd_code_from_claim in (select trim(icd_9) 
                                   from resources.uh_dx_icd_all
                                        where code_description like '%cocaine%'
                                              and code_description not like '%assault%'
                                              and code_description not like '%underdosing%')
		   )
                                              

group by icd_code_from_claim

union

SELECT 
b.icd_code_from_claim as ICDCode,
'Opiod dependency DX' as SubstanceAbuseType,
count(distinct(a.member_uniq_id)) as MemberCount,
count(distinct(a.subscriber_id)) as SubscriberCount,
count(distinct(a.claim_number)) as ClaimCount,
count(distinct(a.ioc)) as IOCCount,
count(distinct(a.service_provider_npi)) as ProviderNPICount,
sum(a.amt_Paid) as TotalPaidAmount,
sum(a.amt_Paid) / count(distinct(a.member_uniq_id)) as AmountPaidPerMember,
sum(a.amt_allowed) as TotalAllowedAmount,
sum(a.amt_allowed) / count(distinct(a.member_uniq_id)) as AmountAllowedPerMember
From tufts_presales.medical_claims_wide a 
  inner join tufts_presales.member_dx_details_long b 
    on a.member_uniq_id = b.member_uniq_id 
      and a.claim_number = b.claim_number 
  inner join tufts_presales.eligibility_wide c on a.member_uniq_id = c.member_uniq_id 
where a.fully_adjudicated = 1 and a.exclude_from_calcs = 0
      and (icd_code_from_claim in (select trim(icd_10) 
                                       from resources.uh_dx_icd_all
                                            where code_description like '%Opioid abuse%'
                                                  or  code_description like '%Opioid dependence%')
		or icd_code_from_claim in (select trim(icd_9) 
                                       from resources.uh_dx_icd_all
                                            where code_description like '%Opioid abuse%'
                                                  or  code_description like '%Opioid dependence%')
		   )

group by icd_code_from_claim

union

SELECT 
b.icd_code_from_claim as ICDCode,
'Benzo and other Sedative dependency DX' as SubstanceAbuseType,
count(distinct(a.member_uniq_id)) as MemberCount,
count(distinct(a.subscriber_id)) as SubscriberCount,
count(distinct(a.claim_number)) as ClaimCount,
count(distinct(a.ioc)) as IOCCount,
count(distinct(a.service_provider_npi)) as ProviderNPICount,
sum(a.amt_Paid) as TotalPaidAmount,
sum(a.amt_Paid) / count(distinct(a.member_uniq_id)) as AmountPaidPerMember,
sum(a.amt_allowed) as TotalAllowedAmount,
sum(a.amt_allowed) / count(distinct(a.member_uniq_id)) as AmountAllowedPerMember
From tufts_presales.medical_claims_wide a 
  inner join tufts_presales.member_dx_details_long b 
    on a.member_uniq_id = b.member_uniq_id 
      and a.claim_number = b.claim_number 
  inner join tufts_presales.eligibility_wide c on a.member_uniq_id = c.member_uniq_id 
where a.fully_adjudicated = 1 and a.exclude_from_calcs = 0
      and (icd_code_from_claim in (select trim(icd_10) 
                                       from resources.uh_dx_icd_all
                                            where code_description like '%Sedative, hypnotic or anxiolytic dependence%'
                                                  or  code_description like '%Sedative, hypnotic or anxiolytic abuse%')
	    or icd_code_from_claim in (select trim(icd_9) 
                                       from resources.uh_dx_icd_all
                                            where code_description like '%Sedative, hypnotic or anxiolytic dependence%'
                                                  or  code_description like '%Sedative, hypnotic or anxiolytic abuse%')
		   )

group by icd_code_from_claim

union

SELECT 
b.icd_code_from_claim as ICDCode,
'Meth and other stimulant dependency DX' as SubstanceAbuseType,
count(distinct(a.member_uniq_id)) as MemberCount,
count(distinct(a.subscriber_id)) as SubscriberCount,
count(distinct(a.claim_number)) as ClaimCount,
count(distinct(a.ioc)) as IOCCount,
count(distinct(a.service_provider_npi)) as ProviderNPICount,
sum(a.amt_Paid) as TotalPaidAmount,
sum(a.amt_Paid) / count(distinct(a.member_uniq_id)) as AmountPaidPerMember,
sum(a.amt_allowed) as TotalAllowedAmount,
sum(a.amt_allowed) / count(distinct(a.member_uniq_id)) as AmountAllowedPerMember
From tufts_presales.medical_claims_wide a 
  inner join tufts_presales.member_dx_details_long b 
    on a.member_uniq_id = b.member_uniq_id 
      and a.claim_number = b.claim_number 
  inner join tufts_presales.eligibility_wide c on a.member_uniq_id = c.member_uniq_id 
where a.fully_adjudicated = 1 and a.exclude_from_calcs = 0
      and (icd_code_from_claim in (select trim(icd_10) 
                                      from resources.uh_dx_icd_all
                                           where code_description like '%stimulant dependence%'
                                                 or  code_description like '%stimulant abuse%')
		or icd_code_from_claim in (select trim(icd_9) 
                                      from resources.uh_dx_icd_all
                                           where code_description like '%stimulant dependence%'
                                                 or  code_description like '%stimulant abuse%')
		  )

group by icd_code_from_claim


####The below lines start the procedure codes...
####the query does not return many results...the patients a checked were caught by above logic for alcohol

union

SELECT 
a.procedure_code as ICDCode,
'Substance abuse ICD-PCS' as SubstanceAbuseType,
count(distinct(a.member_uniq_id)) as MemberCount,
count(distinct(a.subscriber_id)) as SubscriberCount,
count(distinct(a.claim_number)) as ClaimCount,
count(distinct(a.ioc)) as IOCCount,
count(distinct(a.service_provider_npi)) as ProviderNPICount,
sum(a.amt_Paid) as TotalPaidAmount,
sum(a.amt_Paid) / count(distinct(a.member_uniq_id)) as AmountPaidPerMember,
sum(a.amt_allowed) as TotalAllowedAmount,
sum(a.amt_allowed) / count(distinct(a.member_uniq_id)) as AmountAllowedPerMember
From tufts_presales.medical_claims_wide a 
    inner join tufts_presales.eligibility_wide c 
      on a.member_uniq_id = c.member_uniq_id 
where a.fully_adjudicated = 1 and a.exclude_from_calcs = 0
      and procedure_code in (select trim(procedure_code) 
                              from resources.icd10_proc_2018
                               where procedure_code like 'HZ%')

group by a.procedure_code

union

SELECT 
a.procedure_code_2 as ICDCode,
'Substance abuse ICD-PCS' as SubstanceAbuseType,
count(distinct(a.member_uniq_id)) as MemberCount,
count(distinct(a.subscriber_id)) as SubscriberCount,
count(distinct(a.claim_number)) as ClaimCount,
count(distinct(a.ioc)) as IOCCount,
count(distinct(a.service_provider_npi)) as ProviderNPICount,
sum(a.amt_Paid) as TotalPaidAmount,
sum(a.amt_Paid) / count(distinct(a.member_uniq_id)) as AmountPaidPerMember,
sum(a.amt_allowed) as TotalAllowedAmount,
sum(a.amt_allowed) / count(distinct(a.member_uniq_id)) as AmountAllowedPerMember
From tufts_presales.medical_claims_wide a 
    inner join tufts_presales.eligibility_wide c 
      on a.member_uniq_id = c.member_uniq_id 
where a.fully_adjudicated = 1 and a.exclude_from_calcs = 0
      and procedure_code_2 in (select trim(procedure_code) 
                              from resources.icd10_proc_2018
                               where procedure_code like 'HZ%')

group by a.procedure_code_2

union

SELECT 
a.procedure_code_3 as ICDCode,
'Substance abuse ICD-PCS' as SubstanceAbuseType,
count(distinct(a.member_uniq_id)) as MemberCount,
count(distinct(a.subscriber_id)) as SubscriberCount,
count(distinct(a.claim_number)) as ClaimCount,
count(distinct(a.ioc)) as IOCCount,
count(distinct(a.service_provider_npi)) as ProviderNPICount,
sum(a.amt_Paid) as TotalPaidAmount,
sum(a.amt_Paid) / count(distinct(a.member_uniq_id)) as AmountPaidPerMember,
sum(a.amt_allowed) as TotalAllowedAmount,
sum(a.amt_allowed) / count(distinct(a.member_uniq_id)) as AmountAllowedPerMember
From tufts_presales.medical_claims_wide a 
    inner join tufts_presales.eligibility_wide c 
      on a.member_uniq_id = c.member_uniq_id 
where a.fully_adjudicated = 1 and a.exclude_from_calcs = 0
      and procedure_code_3 in (select trim(procedure_code) 
                              from resources.icd10_proc_2018
                               where procedure_code like 'HZ%')

group by a.procedure_code_3

union

SELECT 
a.procedure_code_4 as ICDCode,
'Substance abuse ICD-PCS' as SubstanceAbuseType,
count(distinct(a.member_uniq_id)) as MemberCount,
count(distinct(a.subscriber_id)) as SubscriberCount,
count(distinct(a.claim_number)) as ClaimCount,
count(distinct(a.ioc)) as IOCCount,
count(distinct(a.service_provider_npi)) as ProviderNPICount,
sum(a.amt_Paid) as TotalPaidAmount,
sum(a.amt_Paid) / count(distinct(a.member_uniq_id)) as AmountPaidPerMember,
sum(a.amt_allowed) as TotalAllowedAmount,
sum(a.amt_allowed) / count(distinct(a.member_uniq_id)) as AmountAllowedPerMember
From tufts_presales.medical_claims_wide a 
    inner join tufts_presales.eligibility_wide c 
      on a.member_uniq_id = c.member_uniq_id 
where a.fully_adjudicated = 1 and a.exclude_from_calcs = 0
      and procedure_code_4 in (select trim(procedure_code) 
                              from resources.icd10_proc_2018
                               where procedure_code like 'HZ%')

group by a.procedure_code_4

union

SELECT 
a.procedure_code_5 as ICDCode,
'Substance abuse ICD-PCS' as SubstanceAbuseType,
count(distinct(a.member_uniq_id)) as MemberCount,
count(distinct(a.subscriber_id)) as SubscriberCount,
count(distinct(a.claim_number)) as ClaimCount,
count(distinct(a.ioc)) as IOCCount,
count(distinct(a.service_provider_npi)) as ProviderNPICount,
sum(a.amt_Paid) as TotalPaidAmount,
sum(a.amt_Paid) / count(distinct(a.member_uniq_id)) as AmountPaidPerMember,
sum(a.amt_allowed) as TotalAllowedAmount,
sum(a.amt_allowed) / count(distinct(a.member_uniq_id)) as AmountAllowedPerMember
From tufts_presales.medical_claims_wide a 
    inner join tufts_presales.eligibility_wide c 
      on a.member_uniq_id = c.member_uniq_id 
where a.fully_adjudicated = 1 and a.exclude_from_calcs = 0
      and procedure_code_5 in (select trim(procedure_code) 
                              from resources.icd10_proc_2018
                               where procedure_code like 'HZ%')

group by a.procedure_code_5

union

SELECT 
a.procedure_code_6 as ICDCode,
'Substance abuse ICD-PCS' as SubstanceAbuseType,
count(distinct(a.member_uniq_id)) as MemberCount,
count(distinct(a.subscriber_id)) as SubscriberCount,
count(distinct(a.claim_number)) as ClaimCount,
count(distinct(a.ioc)) as IOCCount,
count(distinct(a.service_provider_npi)) as ProviderNPICount,
sum(a.amt_Paid) as TotalPaidAmount,
sum(a.amt_Paid) / count(distinct(a.member_uniq_id)) as AmountPaidPerMember,
sum(a.amt_allowed) as TotalAllowedAmount,
sum(a.amt_allowed) / count(distinct(a.member_uniq_id)) as AmountAllowedPerMember
From tufts_presales.medical_claims_wide a 
    inner join tufts_presales.eligibility_wide c 
      on a.member_uniq_id = c.member_uniq_id 
where a.fully_adjudicated = 1 and a.exclude_from_calcs = 0
      and procedure_code_6 in (select trim(procedure_code) 
                              from resources.icd10_proc_2018
                               where procedure_code like 'HZ%')

group by a.procedure_code_6

union

SELECT 
a.procedure_code_7 as ICDCode,
'Substance abuse ICD-PCS' as SubstanceAbuseType,
count(distinct(a.member_uniq_id)) as MemberCount,
count(distinct(a.subscriber_id)) as SubscriberCount,
count(distinct(a.claim_number)) as ClaimCount,
count(distinct(a.ioc)) as IOCCount,
count(distinct(a.service_provider_npi)) as ProviderNPICount,
sum(a.amt_Paid) as TotalPaidAmount,
sum(a.amt_Paid) / count(distinct(a.member_uniq_id)) as AmountPaidPerMember,
sum(a.amt_allowed) as TotalAllowedAmount,
sum(a.amt_allowed) / count(distinct(a.member_uniq_id)) as AmountAllowedPerMember
From tufts_presales.medical_claims_wide a 
    inner join tufts_presales.eligibility_wide c 
      on a.member_uniq_id = c.member_uniq_id 
where a.fully_adjudicated = 1 and a.exclude_from_calcs = 0
      and procedure_code_7 in (select trim(procedure_code) 
                              from resources.icd10_proc_2018
                               where procedure_code like 'HZ%')

group by a.procedure_code_7

union

SELECT 
a.procedure_code_8 as ICDCode,
'Substance abuse ICD-PCS' as SubstanceAbuseType,
count(distinct(a.member_uniq_id)) as MemberCount,
count(distinct(a.subscriber_id)) as SubscriberCount,
count(distinct(a.claim_number)) as ClaimCount,
count(distinct(a.ioc)) as IOCCount,
count(distinct(a.service_provider_npi)) as ProviderNPICount,
sum(a.amt_Paid) as TotalPaidAmount,
sum(a.amt_Paid) / count(distinct(a.member_uniq_id)) as AmountPaidPerMember,
sum(a.amt_allowed) as TotalAllowedAmount,
sum(a.amt_allowed) / count(distinct(a.member_uniq_id)) as AmountAllowedPerMember
From tufts_presales.medical_claims_wide a 
    inner join tufts_presales.eligibility_wide c 
      on a.member_uniq_id = c.member_uniq_id 
where a.fully_adjudicated = 1 and a.exclude_from_calcs = 0
      and procedure_code_8 in (select trim(procedure_code) 
                              from resources.icd10_proc_2018
                               where procedure_code like 'HZ%')

group by a.procedure_code_8

union

SELECT 
a.procedure_code_9 as ICDCode,
'Substance abuse ICD-PCS' as SubstanceAbuseType,
count(distinct(a.member_uniq_id)) as MemberCount,
count(distinct(a.subscriber_id)) as SubscriberCount,
count(distinct(a.claim_number)) as ClaimCount,
count(distinct(a.ioc)) as IOCCount,
count(distinct(a.service_provider_npi)) as ProviderNPICount,
sum(a.amt_Paid) as TotalPaidAmount,
sum(a.amt_Paid) / count(distinct(a.member_uniq_id)) as AmountPaidPerMember,
sum(a.amt_allowed) as TotalAllowedAmount,
sum(a.amt_allowed) / count(distinct(a.member_uniq_id)) as AmountAllowedPerMember
From tufts_presales.medical_claims_wide a 
    inner join tufts_presales.eligibility_wide c 
      on a.member_uniq_id = c.member_uniq_id 
where a.fully_adjudicated = 1 and a.exclude_from_calcs = 0
      and procedure_code_9 in (select trim(procedure_code) 
                              from resources.icd10_proc_2018
                               where procedure_code like 'HZ%')

group by a.procedure_code_9

union

SELECT 
a.procedure_code_10 as ICDCode,
'Substance abuse ICD-PCS' as SubstanceAbuseType,
count(distinct(a.member_uniq_id)) as MemberCount,
count(distinct(a.subscriber_id)) as SubscriberCount,
count(distinct(a.claim_number)) as ClaimCount,
count(distinct(a.ioc)) as IOCCount,
count(distinct(a.service_provider_npi)) as ProviderNPICount,
sum(a.amt_Paid) as TotalPaidAmount,
sum(a.amt_Paid) / count(distinct(a.member_uniq_id)) as AmountPaidPerMember,
sum(a.amt_allowed) as TotalAllowedAmount,
sum(a.amt_allowed) / count(distinct(a.member_uniq_id)) as AmountAllowedPerMember
From tufts_presales.medical_claims_wide a 
    inner join tufts_presales.eligibility_wide c 
      on a.member_uniq_id = c.member_uniq_id 
where a.fully_adjudicated = 1 and a.exclude_from_calcs = 0
      and procedure_code_10 in (select trim(procedure_code) 
                              from resources.icd10_proc_2018
                               where procedure_code like 'HZ%')

group by a.procedure_code_10