SELECT 
b.icd_code_from_claim,
count(distinct(a.member_uniq_id)) as MemberCount,
count(distinct(a.subscriber_id)) as SubscriberCount,
count(distinct(a.claim_number)) as ClaimCount,
count(distinct(a.ioc)) as IOCCount,
count(distinct(a.service_provider_npi)) as ProviderNPICount,
sum(a.amt_billed) as TotalBilledAmount,
sum(a.amt_billed) / count(distinct(a.member_uniq_id)) as AmountBilledPerMember,
sum(a.amt_allowed) as TotalAllowedAmount,
sum(a.amt_allowed) / count(distinct(a.member_uniq_id)) as AmountAllowedPerMember
From tufts_presales.medical_claims_wide a 
  inner join tufts_presales.all_members_all_dx_full b 
    on a.member_uniq_id = b.member_uniq_id 
      and a.claim_number = b.claim_number 
  inner join tufts_presales.eligibility_wide c on a.member_uniq_id = c.member_uniq_id 
where a.fully_adjudicated = 1 and a.exclude_from_calcs = 0
#and c.control_group = 0
#and upper(c.group_id_prefix) in ('BCBSTX', 'BCBSIL')
and b.isPrimary = 1
group by icd_code_from_claim