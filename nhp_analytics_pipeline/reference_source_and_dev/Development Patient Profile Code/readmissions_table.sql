drop table member_presales_readmissions_final;

CREATE TABLE `member_presales_readmissions_final` (
  `member_uniq_id` varchar(255) DEFAULT NULL,
  `ioc` varchar(255) DEFAULT NULL,
  `service_provider_npi` varchar(10) DEFAULT NULL,
  `service_start_date` date DEFAULT NULL,
  `service_end_date` date DEFAULT NULL,
  KEY `ioc` (`ioc`),
  KEY `member_uniq_id` (`member_uniq_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


insert into member_presales_readmissions_raw 


select distinct
member_uniq_id,
ioc,
service_provider_npi,
service_start_date,
service_end_date
from member_presales_claims
where admit_ind = 'Y'
order by member_uniq_id, service_start_date, service_provider_npi
;



