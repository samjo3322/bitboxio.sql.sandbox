select 
count(distinct(a.member_uniq_id)) as MemberCount,
count(distinct(a.subscriber_id)) as SubscriberCount,
count(distinct(a.claim_number)) as ClaimCount,
count(distinct(a.ioc)) as IOCCount,
count(distinct(a.service_provider_npi)) as ProviderNPICount,
sum(a.amt_paid) as TotalPaidAmount,
sum(a.amt_paid) / count(distinct(a.member_uniq_id)) as AmountPaidPerMember,
sum(a.amt_allowed) as TotalAllowedAmount,
sum(a.amt_allowed) / count(distinct(a.member_uniq_id)) as AmountAllowedPerMember
from tufts_presales.medical_claims_wide a 
inner join tufts_presales.eligibility_wide b 
	on a.member_uniq_id = b.member_uniq_id
		and service_start_date 
			between eligibility_start_date 
				and eligibility_end_date
Where a.fully_adjudicated = 1 and a.exclude_from_calcs = 0
   and (     
		  (left(a.procedure_code_2,1) = 'B' or
           left(a.procedure_code_3,1) = 'B' or
           left(a.procedure_code_4,1) = 'B' or
           left(a.procedure_code_5,1) = 'B' or
           left(a.procedure_code_6,1) = 'B' or
           left(a.procedure_code_7,1) = 'B' or
           left(a.procedure_code_8,1) = 'B' or
           left(a.procedure_code_9,1) = 'B' or
           left(a.procedure_code_10,1) = 'B' )
        or cast(a.procedure_code as SIGNED) between 70010 and 79999 
        or rev_code in ('320', '321', '322', '323', '324', '329', '330', '331', '332', '333', '335', '339')
        )

