DELIMITER $$
CREATE PROCEDURE `readmissions_procedure`(i_window int, -- number of days that create a violation window
                                       i_target int, -- number of violations required to trigger identification
                                       i_rule_name varchar(255))
BEGIN
###declare variables used in the program
  
  declare v_row_count int default 0;
  declare v_startdate_1 date default null;
  declare v_startdate_2 date default null;
  declare v_enddate_1 date default null;
  declare v_enddate_2 date default null;
  declare v_member_1 varchar(255) default null;
  declare v_member_2 varchar(255) default null;
  declare done int;
  declare v_target_count int default 0;
  declare v_days int default 0;
  declare v_hold_claim_numbers longtext;
  declare v_claim_number date default null;
  declare v_created_at datetime;
  declare v_concat_ind int default 0;
 
  ###declare cursor and load data into it
  DECLARE claim_date CURSOR FOR
  SELECT distinct member_uniq_id,
                  service_start_date,
                  service_end_date
  From tufts_presales.member_presales_claims
 # where member_uniq_id = 'N1103104293'
  order by member_uniq_id, service_start_date asc, service_end_date asc;

###open cursor and begin program
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
open claim_date;
  claim_loop: loop
    if done = 1 then
      close claim_date;
      leave claim_loop;
    end if;  

###set created date
set v_created_at = sysdate();

###fetch data from cursor
fetch claim_date into v_member_1, v_startdate_1, v_enddate_1;
  set v_row_count = v_row_count + 1;
  
###begin program goal is to store the first row of data into variables
if v_row_count = 1 then
	set v_member_2 = v_member_1;
    set v_startdate_2 = v_startdate_1;
    set v_enddate_2 = v_enddate_1;
else if  v_row_count > 1 and v_member_1 = v_member_2 then   
set v_days = 0;
    set v_days = datediff(v_startdate_1,v_enddate_2);
		if v_days < i_window then 
		set v_target_count = v_target_count + 1;
        end if;
	if v_days < 0 then
        set v_member_2 = v_member_1;
		set v_startdate_2 = v_startdate_1;
		set v_enddate_2 = v_enddate_1;
    else if v_days > i_window then
        set v_member_2 = v_member_1;
		set v_startdate_2 = v_startdate_1;
		set v_enddate_2 = v_enddate_1;
    else if v_days >= 0 and v_days <= i_window then
insert into tufts_presales.member_presales_readmissions
				(member_uniq_id, rule_name, claim_string, trigger_count, created_at)
				values (v_member_1,v_days,v_startdate_1,v_row_count,v_created_at);
    set v_member_2 = v_member_1;
    set v_startdate_2 = v_startdate_1;
    set v_enddate_2 = v_enddate_1;
    else
    set v_member_2 = v_member_1;
    set v_startdate_2 = v_startdate_1;
    set v_enddate_2 = v_enddate_1;
    end if;
    end if;
    end if;
else 
    set v_member_2 = v_member_1;
    set v_startdate_2 = v_startdate_1;
    set v_enddate_2 = v_enddate_1;
  end if;
end if;
end loop claim_loop;	
END