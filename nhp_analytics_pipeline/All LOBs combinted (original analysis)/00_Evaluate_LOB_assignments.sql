select count(distinct Carrier_Specific_Unique_Member_ID), Insurance_Type_Code_Product
from nhp.medical_raw
group by Insurance_Type_Code_Product;

select count(distinct Carrier_Specific_Unique_Member_ID), Insurance_Type_Code_Product
from nhp.rx_raw
group by Insurance_Type_Code_Product;

select count(distinct Carrier_Specific_Unique_Member_ID), Insurance_Type_Code_Product
from nhp.eligibility_raw
group by Insurance_Type_Code_Product;

-- look at eligibility and updates (do we need join based on eligibility start/end dates and date filled?)

select Carrier_Specific_Unique_Member_ID,
       Product_Enrollment_Start_Date,
       Product_Enrollment_End_Date,
       Insurance_Type_Code_Product,
	   count(distinct Insurance_Type_Code_Product)
from nhp.eligibility_raw
group by Carrier_Specific_Unique_Member_ID
having 	   count(distinct Insurance_Type_Code_Product) > 1;

select Carrier_Specific_Unique_Member_ID,
       Product_Enrollment_Start_Date,
       Product_Enrollment_End_Date,
       Insurance_Type_Code_Product
from nhp.eligibility_raw
where Carrier_Specific_Unique_Member_ID in ('1000004288',
'1000006147',
'1000008749',
'1000010286');

-- so it does vary over time, but has that been accounted for in the claims?

select Carrier_Specific_Unique_Member_ID,
	   count(distinct Insurance_Type_Code_Product)
from nhp.medical_raw
group by Carrier_Specific_Unique_Member_ID
having count(distinct Insurance_Type_Code_Product) > 1;

-- see what numbers are pulled from above query and compare against the above data to understand if the values are properly udpated in claims or just current state

select distinct Carrier_Specific_Unique_Member_ID,
       Date_of_Service_From,
       Date_of_Service_to,
       Insurance_Type_Code_Product
from nhp.medical_raw
where Carrier_Specific_Unique_Member_ID in ('1000044182',
'1000045798',
'1000046689',
'1000049090')
order by Carrier_Specific_Unique_Member_ID, Date_of_Service_From;

select distinct Carrier_Specific_Unique_Member_ID,
       Product_Enrollment_Start_Date,
       Product_Enrollment_End_Date,
       Insurance_Type_Code_Product
from nhp.eligibility_raw
where Carrier_Specific_Unique_Member_ID in ('1000044182',
'1000045798',
'1000046689',
'1000049090')
order by Carrier_Specific_Unique_Member_ID, Product_Enrollment_Start_Date;

-- checks out on MD.

select Carrier_Specific_Unique_Member_ID,
	   count(distinct Insurance_Type_Code_Product)
from nhp.medical_raw
group by Carrier_Specific_Unique_Member_ID
having count(distinct Insurance_Type_Code_Product) > 1;

-- see what numbers are pulled from above query and compare against the above data to understand if the values are properly udpated in claims or just current state

select distinct Carrier_Specific_Unique_Member_ID,
       Date_Prescription_Filled,
       Insurance_Type_Code_Product
from nhp.rx_raw
where Carrier_Specific_Unique_Member_ID in ('1000044182',
'1000045798',
'1000046689',
'1000049090')
order by Carrier_Specific_Unique_Member_ID, Date_Prescription_Filled;

select distinct Carrier_Specific_Unique_Member_ID,
       Product_Enrollment_Start_Date,
       Product_Enrollment_End_Date,
       Insurance_Type_Code_Product
from nhp.eligibility_raw
where Carrier_Specific_Unique_Member_ID in ('1000044182',
'1000045798',
'1000046689',
'1000049090')
order by Carrier_Specific_Unique_Member_ID, Product_Enrollment_Start_Date;

-- looks good for RX