select conditions,
       condition_count,
       a.member_uniq_id,
       lob,
       a.claim_number,
       a.claim_line_number,
	   a.date_of_service,
       a.service_start_date,
       a.service_end_date,
       a.amt_allowed,
       a.amt_paid,
       cms_place_of_service_id,
       pos.name,
       procedure_code,
       cpt.description,
       DRG,
       drg.MS_DRG_Title,
       rev_code,
       rev.description,
       service_units,
       claim_status,
       admit_ind,
       icd_1,
       icd1.description,
       icd_2,
       icd2.description,
       icd_3,
       icd3.description,
       icd_4,
       icd4.description,
       icd_5,
       icd5.description,
       icd_6,
       icd6.description,
       icd_7,
       icd7.description,
       icd_8,
       icd8.description,
       icd_9,
       icd9.description,
       icd_10,
       icd10.description,
	   case when a.date_of_service between '2017-07-01' and '2018-06-30' then 2018
            when a.date_of_service between '2016-07-01' and '2017-06-30' then 2017
            when a.date_of_service between '2015-07-01' and '2016-06-30' then 2016 end as rolling_year
from medical_claims_wide a
  join temp_conditions b
    on a.member_uniq_id = b.member
  left join resources.cpt_common cpt
    on a.procedure_code = cpt.Code
  left join resources.drg drg
    on a.drg = drg.`MS_DRG` -- check to see if needs padding for join
  left join resources.rev_codes rev
    on a.rev_code = rev.revenue_code
  left join resources.icd10_2017 icd1
    on ifnull(icd_1,'') = icd1.code
  left join resources.icd10_2017 icd2
    on ifnull(icd_2,'') = icd2.code
  left join resources.icd10_2017 icd3
    on ifnull(icd_3,'') = icd3.code
  left join resources.icd10_2017 icd4
    on ifnull(icd_4,'') = icd4.code
  left join resources.icd10_2017 icd5
    on ifnull(icd_5,'') = icd5.code
  left join resources.icd10_2017 icd6
    on ifnull(icd_6,'') = icd6.code
  left join resources.icd10_2017 icd7
    on ifnull(icd_7,'') = icd7.code 
  left join resources.icd10_2017 icd8
    on ifnull(icd_8,'') = icd8.code 
  left join resources.icd10_2017 icd9
    on ifnull(icd_9,'') = icd9.code
  left join resources.icd10_2017 icd10
    on ifnull(icd_10,'') = icd10.code
  left join resources.pos pos
    on ifnull(cms_place_of_service_id,'') = pos.pos
where date_of_service between '2015-07-01' and '2018-06-30'
and a.member_uniq_id in ('776157571   02','889639089   02','775172682   05','776843613   01','N1794033493','N1155338193','A18992752   01','N1716578993','776894436   02',
                         'N1152204493')
order by conditions, claim_number, claim_line_number, date_of_service;

-- POS Profile

drop table if exists pos_profile_claims;
create table pos_profile_claims
(
member_uniq_id varchar(22),
cms_place_of_service_id varchar(2),
procedure_code varchar(10),
admit_ind varchar(1),
amt_paid decimal(14,2),
amt_allowed decimal(14,2),
claim_status varchar(1),
claim_number varchar(55),
date_of_service date,
service_start_date date,
service_end_date date,
key member_uniq_id (member_uniq_id(12)),
key cms_place_of_service_id (cms_place_of_service_id(2)),
key claim_number (claim_number(12))
);

alter table pos_profile_claims add index procedure_code (procedure_code(8));

insert into pos_profile_claims
select member_uniq_id,
       cms_place_of_service_id,
       procedure_code,
       admit_ind,
       amt_paid,
       amt_allowed,
       claim_status,
       claim_number,
       date_of_service,
       service_start_date,
       service_end_date
from medical_claims_wide a
  join temp_cofounding_members b
    on a.member_uniq_id = b.member
where date_of_service between '2017-07-01' and '2018-06-30'
  and lob = 'Public';

-- Ad Hoc requests

-- number of ED visits
select count(distinct concat(service_start_date,'|',service_end_date,'|', member_uniq_id))
from pos_profile_claims
where cms_place_of_service_id = 23;

-- What are the top 5 dx and/or reasons this group went to the Ed?
select count(distinct claim_number), code_description 
from member_dx_details_long
where claim_number in (select claim_number from pos_profile_claims
                       where cms_place_of_service_id = 23)
 and code_description is not null
group by code_description
order by count(distinct claim_number) desc;

-- How many inpatient admissions in this group (11,405)
select count(distinct concat(service_start_date,'|',service_end_date,'|', member_uniq_id))
from pos_profile_claims
where admit_ind = 'Y';

select count(distinct member_uniq_id), count(distinct claim_number)
from pos_profile_claims
where admit_ind = 'Y';

-- What are the top 5 dx and/or reasons for admission?

select count(distinct claim_number), code_description 
from member_dx_details_long
where claim_number in (select claim_number from pos_profile_claims) -- where admit_ind = 'Y')
 and code_description is not null
group by code_description
order by count(distinct claim_number) desc;

-- what are the top 5 diagnostics done for this population in the OP setting?
  -- skipping; super complicated

-- What are the top 5 procedures done for this population in the OP setting?
select count(*), cpt.description
from pos_profile_claims a
  join resources.cpt_common cpt
    on a.procedure_code = cpt.code
where cms_place_of_service_id in (11,19,20,22)
group by cpt.description
order by count(*) desc;

