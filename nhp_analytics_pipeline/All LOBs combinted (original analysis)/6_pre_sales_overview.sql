/* Data Quality Checks */

-- medical overview: What do claims look like?
truncate table medical_overview;
insert into medical_overview
select count(*) as rowCount,
       count(distinct claim_number) as claimsCount,
       sum(case when claim_status = 'P' then 1
                else 0 end) as paidClaimsCount,
	   count(distinct member_uniq_id) as memberCount,
       sum(amt_paid) as totalSpend,
       sum(amt_allowed) as totalExposure,
       count(distinct cms_place_of_service_id) as posCount,
       count(distinct icd_1) as icd_1Count,
       count(distinct icd_2) as icd_2Count,
       count(distinct icd_3) as icd_3Count,
       count(distinct icd_4) as icd_4Count,
       count(distinct icd_5) as icd_5Count,
       count(distinct icd_6) as icd_6Count,
       count(distinct icd_7) as icd_7Count,
       count(distinct icd_8) as icd_8Count,
       count(distinct icd_9) as icd_9Count,
       count(distinct icd_10) as icd_10Count,
       count(distinct procedure_code) as procedureCount,
       count(distinct rev_code) as revCodeCount,
       count(distinct DRG) as DRGCount,
       min(date_of_service) as EariestDateOfService,
       max(date_of_service) as LatestDateOfService
from medical_claims_wide;

-- medical financial reconcillaition: Do the financial fields reconcile and what is the volume of mismatches?
truncate table medical_financial_check;
insert into medical_financial_check
select *,
       badClaimsCount/allClaimsCount as badClaimsPercent,
       badMemberCount/allMemberCount as badMemberPercent,
       badRowCount/allRowCount as badRowPercent,
       badTotalSpend/allTotalSpend as badSpendPercent,
       badTotalExposure/allTotalExposure as badExposurePercent
from
  (select count(distinct claim_number) as badClaimsCount,
          count(*) as badRowCount,
          count(distinct member_uniq_id) as badMemberCount,
          sum(amt_paid) as badTotalSpend,
        sum(amt_allowed) as badTotalExposure
   from medical_claims_wide
   where ifnull(amt_paid,0)+ifnull(amt_deductible,0)+ifnull(amt_copay,0)+ifnull(amt_cob,0) != amt_allowed
  ) bad
join
  (select count(distinct claim_number) as allClaimsCount,
          count(*) as allRowCount,
          count(distinct member_uniq_id) as allMemberCount,
          sum(amt_paid) as allTotalSpend,
        sum(amt_allowed) as allTotalExposure
   from medical_claims_wide
  ) overall;

-- POS Counts: What does this distribution look like?
truncate table pos_overview;
insert into pos_overview
select count(distinct claim_number) as claimsCount,
       count(distinct member_uniq_id) as memberCount,
       claims.cms_place_of_service_id as cmsID,
       place.name as pos_name
from medical_claims_wide claims
  join resources.pos place
    on claims.cms_place_of_service_id = place.pos
group by claims.cms_place_of_service_id,
         place.name;
         
-- ICD distribution
  -- counts by category
truncate table icd_counts_by_category;
insert into icd_counts_by_category
select count(distinct member_uniq_id) as memberCount,
       count(distinct claim_number) as claimCount,
       category
from member_dx_details_long
group by category;

-- rx overview
truncate table rx_overview;
insert into rx_overview
select count(*) as rowCount,
       count(distinct claim_number) as claimsCount,
       sum(case when claim_status = 'P' then 1
                else 0 end) as paidClaimsCount,
	   count(distinct member_uniq_id) as memberCount,
       sum(amt_paid) as totalSpend,
       sum(amt_allowed) as totalExposure,
       count(distinct NDC) as totalNDCCount,
       sum(case when GDI_number is null then 1
                else 0 end) as nonMatchedNDCClaimsCount,
	   (select count(distinct NDC) from rx_claims where GDI_number is null) as nonMatchedNDCCodesCount,
       count(distinct misc_3) as pharmacyNPICount,
       count(distinct prescribing_provider_npi) as prescribingProviderCount,
       min(date_filled) as EariestDateFilled,
       max(date_filled) as LatestDateFilled
from rx_claims;

-- rx financial reconcilliation
truncate table rx_financial_check;
insert into rx_financial_check
select *,
       badClaimsCount/allClaimsCount as badClaimsPercent,
       badMemberCount/allMemberCount as badMemberPercent,
       badRowCount/allRowCount as badRowPercent,
       badTotalSpend/allTotalSpend as badSpendPercent,
       badTotalExposure/allTotalExposure as badExposurePercent
from
  (select count(distinct claim_number) as badClaimsCount,
          count(*) as badRowCount,
          count(distinct member_uniq_id) as badMemberCount,
          sum(amt_paid) as badTotalSpend,
        sum(amt_allowed) as badTotalExposure
   from rx_claims
   where ifnull(amt_paid,0)+ifnull(amt_deductible,0)+ifnull(amt_copay,0)+ifnull(cob_amt,0) != amt_allowed
  ) bad
join
  (select count(distinct claim_number) as allClaimsCount,
          count(*) as allRowCount,
          count(distinct member_uniq_id) as allMemberCount,
          sum(amt_paid) as allTotalSpend,
        sum(amt_allowed) as allTotalExposure
   from rx_claims
  ) overall;
  
-- rx NDC drug type overview
truncate table rx_drug_type_overview;
insert into rx_drug_type_overview
select case when ndc_generic is null then 'No NDC Match'
            else ndc_generic end as brandOrGeneric,        
       case when ndc_maintenance is null then 'No NDC Match'
            else ndc_maintenance end as maintainenceIndicator,
       count(*) as Counts
from rx_claims
group by ndc_generic,
         ndc_maintenance;

-- Eligibility overview:
truncate table eligibility_overview;
insert into eligibility_overview
select count(*) as totalRows,
       (select count(*) from eligibility_wide where active_record = 1) as totalActiveRows,
       count(distinct member_uniq_id) as memberCount,
       count(distinct subscriber_id) as susbcriberCount,
       count(distinct group_id_base) as groupCount,
       count(distinct subscriber_relationship) as relationshipCount,
       min(program_beg_date) as earliestEligigbilityRecord,
       max(program_end_date) as latestEligibilityRecord,
       (select min(program_beg_date) from eligibility_wide where active_record = 1) as earliestActiveRecord,
       (select max(program_end_date) from eligibility_wide where active_record = 1) as latestActiveRecord
from eligibility_wide;
       
-- any members with more than 1 active row? Should return no results or ETL needs to be examined
truncate table active_row_check;
insert into active_row_check
select member_uniq_id,
       count(*)
from eligibility_wide
where active_record = 1
group by member_uniq_id
having count(*) > 1;

/* Other metrics to to check */

truncate table member_reconcilation_check;
insert into member_reconcilation_check
-- members in medical claims not in the eligibility file
select count(distinct med.member_uniq_id),
       'Members in Medical Claims not in ELigibility'
from medical_claims_wide med
  left join eligibility_wide elig
      on med.member_uniq_id = elig.member_uniq_id
where elig.member_uniq_id is null
  and active_record = 1
union all
select count(distinct rx.member_uniq_id),
       'Members in RX Claims not in ELigibility'
from rx_claims rx
  left join eligibility_wide elig
      on rx.member_uniq_id = elig.member_uniq_id
where elig.member_uniq_id is null
  and active_record = 1
union all
-- member ID in MD/RX overlap
select count(distinct med.member_uniq_id),
       'Members in Medical Claims not in RX'
from medical_claims_wide med
  left join rx_claims rx
      on med.member_uniq_id = rx.member_uniq_id
where rx.member_uniq_id is null
union all
select count(distinct rx.member_uniq_id),
       'Members in RX Claims not in Medical'
from rx_claims rx
  left join medical_claims_wide med
      on rx.member_uniq_id = med.member_uniq_id
where med.member_uniq_id is null
union all
-- see how many members may have eligibility issues with active row assignment
select count(distinct member_uniq_id),
       'Members without an active record'
from eligibility_wide
where member_uniq_id not in (select distinct member_uniq_id 
                             from eligibility_wide 
                             where active_record = 1)
union all
-- how many members who have no claims with an ICD code in our internal DX details table
select count(distinct member_uniq_id),
       'Members with no claim in UH internal ICD list'
from member_dx_details_long
where member_uniq_id not in (select distinct member_uniq_id
							 from member_dx_details_long
                             where joined = 1);
                             
-- Per member spend
truncate table per_member_spend;
insert into per_member_spend
select sum(amt_paid_med) / count(distinct member_uniq_id) as medicalPerMemberSpend,
       sum(amt_paid_rx) / count(distinct member_uniq_id) as rxPerMemberSpend,
       sum(totalPaid) / count(distinct member_uniq_id) as totalPerMemberSpend
from members_spend;

select * from medical_overview;
select * from medical_financial_check;
select * from pos_overview;
select * from icd_counts_by_category;
select * from rx_overview;
select * from rx_financial_check;
select * from rx_drug_type_overview;
select * from eligibility_overview;
select * from active_row_check;
select * from member_reconcilation_check;
select * from per_member_spend;

