/* Eligibility */

truncate table nhp.eligibility_wide;
truncate table nhp.rx_claims;
truncate table nhp.medical_claims_wide;

insert into nhp.eligibility_wide
(report_date,group_id_base,member_uniq_id,subscriber_relationship,member_gender,member_dob,member_state,member_zip,program_beg_date,program_end_date,eligibility_start_date,eligibility_end_date,
subscriber_last_name,subscriber_first_name,member_first_name,member_last_name, raw_file_name, lob)
select distinct -- use distinct to limit the number of rows containing duplicative information needed for our analysis
str_to_date(concat(trim(Year),'-',trim(Month),'-','01'),'%Y-%m-%d') as report_date,
nullif(trim(Insured_Group_or_Policy_Number),'') as group_id_base,
nullif(trim(Carrier_Specific_Unique_Member_ID),'') as member_uniq_id,
nullif(case when trim(Individual_Relationship_Code) = '1' then 'Spouse'
     when trim(Individual_Relationship_Code) = '20' then 'Employee/Self'
	 when trim(Individual_Relationship_Code) = '76' then 'Dependent'
     when trim(Individual_Relationship_Code) = '21' then 'Unknown'
     when trim(Individual_Relationship_Code) is null then null
     else 'Check ETL mappings' end,'') as subscriber_relationship,
nullif(trim(Member_Gender),'') as member_gender,
nullif(str_to_date(trim(member_date_of_Birth),'%Y%m%d'),'') as member_dob,
nullif(trim(member_state),'') as member_state,
nullif(trim(Member_ZIP_Code),'') as member_zip,
nullif(str_to_date(trim(Product_Enrollment_Start_Date),'%Y%m%d'),'') as program_beg_date,
nullif(str_to_date(trim(Product_Enrollment_End_Date),'%Y%m%d'),'') as program_end_date,
nullif(str_to_date(trim(Product_Enrollment_Start_Date),'%Y%m%d'),'') as eligibility_start_date,
nullif(str_to_date(trim(Product_Enrollment_End_Date),'%Y%m%d'),'') as eligibility_end_date,
nullif(trim(Subscriber_Last_Name),'') as subscriber_last_name,
nullif(trim(Subscriber_First_Name),'') as subscriber_first_name,
nullif(trim(Member_First_Name),'') as member_first_name,
nullif(trim(Member_Last_Name),'') as member_last_name,
'20150101_20180831_APCD_Eligibility.txt' as raw_file_name,
  case when nullif(trim(Insurance_Type_Code_Product),'') = '12' then 'Commercial-PPO'
       when nullif(trim(Insurance_Type_Code_Product),'') = '30' then 'Commercial-ACO'
       when nullif(trim(Insurance_Type_Code_Product),'') = 'HM' then 'Commercial'
       when nullif(trim(Insurance_Type_Code_Product),'') = 'MC' then 'Medicaid'
       when nullif(trim(Insurance_Type_Code_Product),'') = 'CC' then 'Medicaid-CCHIP' end as lob
from eligibility_raw;

update eligibility_wide
set program_end_date = '2099-12-31'
where program_end_date = '0000-00-00';

update eligibility_wide
set active_record = 0;

-- of the duplicative rows remaining, identify the current record
update nhp.eligibility_wide elig, 
	   (select max(program_end_date) as max_end_date,
                             member_uniq_id
                             from eligibility_wide
                             group by member_uniq_id) active1,
	   (select max(program_beg_date) as max_start_date,
                             member_uniq_id
                             from eligibility_wide
                             group by member_uniq_id) active2                    
set active_record = 1
where elig.member_uniq_id = active1.member_uniq_id
  and elig.program_end_date = active1.max_end_date
  and elig.member_uniq_id = active2.member_uniq_id
  and elig.program_beg_date = active2.max_start_date;

/* Medical */

INSERT INTO nhp.medical_claims_wide
(uh_payer_id,lob,claim_number,claim_line_number,member_uniq_id,admit_ind,service_provider_id,service_provider_tin,service_provider_npi,service_provider_name,
service_provider_city,service_provider_state,service_provider_zip,cms_place_of_service_id,claim_status,Misc_1,misc_1_desc,misc_3,misc_3_desc,Icd_1,Icd_2,Icd_3,Icd_4,Icd_5,Icd_6,Icd_7,Icd_8,
Icd_9,Icd_10,rev_code,procedure_code,procedure_modifier,Procedure_code_2,procedure_modifier_2,service_start_date,service_end_date,date_of_service,service_units,amt_billed,amt_paid,amt_deductible,
amt_copay,amt_coinsurance,DRG,billing_provider_NPI,procedure_code_3,procedure_code_4,procedure_code_5,procedure_code_6,procedure_code_7,procedure_code_8,date_paid,amt_cob,amt_allowed,icd_version,
misc_2,misc_2_desc,amt_patient_responsibility,network,misc_4, misc_4_desc,raw_file_name)

select
  nullif(trim(National_Plan_ID),'') as uh_payer_id,
  case when nullif(trim(Insurance_Type_Code_Product),'') = '12' then 'Commercial-PPO'
       when nullif(trim(Insurance_Type_Code_Product),'') = '30' then 'Commercial-ACO'
       when nullif(trim(Insurance_Type_Code_Product),'') = 'HM' then 'Commercial'
       when nullif(trim(Insurance_Type_Code_Product),'') = 'MC' then 'Medicaid'
       when nullif(trim(Insurance_Type_Code_Product),'') = 'CC' then 'Medicaid-CCHIP' end as lob,
  nullif(trim(Payer_Claim_Control_Number),'') as claim_number,
  nullif(trim(Line_Counter),'')as claim_line_number,
  nullif(trim(Carrier_Specific_Unique_Member_ID),'') as member_uniq_id,
  case when ifnull(trim(Admission_date),'') != '' then 'Y'
              else 'N' end as admit_ind,
  nullif(trim(Service_Provider_Number),'') as service_provider_id,
  nullif(trim(Service_Provider_Tax_ID_Number),'') as service_provider_tin,
  case when trim(National_Provider_ID_Service) = '1111111111' then null
       when trim(National_Provider_ID_Service) like '123456789%' then null
       when trim(National_Provider_ID_Service) = '9999999999' then null 
       when length(trim(National_Provider_ID_Service)) != 10 then null
       else nullif(trim(National_Provider_ID_Service),'') end as service_provider_npi,
  nullif(trim(concat(ifnull(trim(Service_Provider_First_Name),''),' ',ifnull(trim(Service_Provider_Middle_Initial),''),' ',ifnull(trim(Servicing_Provider_Last_Name_or_Organization_Name),''))),'') as service_provider_name,
  nullif(trim(Service_Provider_City_Name),'') as service_provider_city,
  nullif(trim(Service_Provider_State),'') as service_provider_state,
  nullif(trim(Service_Provider_ZIP_Code),'') as service_provider_zip,
  nullif(trim(Site_of_Service_on_NSF_CMS_1500_Claims),'') as cms_place_of_service_id,
  nullif(case when trim(Claim_Status) = '4' then 'D'
	     when trim(Claim_Status) = '22' then 'V'
         when trim(Claim_Status) is null then null
         else 'P' end,'') as claim_status,
  nullif(trim(Admitting_Diagnosis),'') as misc_1,
  'Admitting_Diagnosis' as misc_1_desc,
  str_to_date(trim(Admission_Date),'%Y%m%d') as misc_3,
  'Admission_Date' as misc_3_desc,
  nullif(trim(Principal_Diagnosis),'') as Icd_1,
  nullif(trim(Other_Diagnosis_1),'') as Icd_2,
  nullif(trim(Other_Diagnosis_2),'') as Icd_3,
  nullif(trim(Other_Diagnosis_3),'') as Icd_4,
  nullif(trim(Other_Diagnosis_4),'') as Icd_5,
  nullif(trim(Other_Diagnosis_5),'') as Icd_6,
  nullif(trim(Other_Diagnosis_6),'') as Icd_7,
  nullif(trim(Other_Diagnosis_7),'') as Icd_8,
  nullif(trim(Other_Diagnosis_8),'') as Icd_9,
  nullif(trim(Other_Diagnosis_9),'') as Icd_10,
  nullif(trim(Revenue_Code),'') as rev_code,
  nullif(trim(Procedure_Code),'') as procedure_code,
  nullif(trim(Procedure_Modifier_1),'') as procedure_modifier,
  nullif(trim(Procedure_Modifier_2),'') as procedure_modifier_2,
  nullif(trim(ICD_CM_Primary_Procedure_Code),'') as Procedure_code_2,
  nullif(str_to_date(trim(Date_of_Service_From),'%Y%m%d'),'') as service_start_date,
  nullif(str_to_date(trim(Date_of_Service_To),'%Y%m%d'),'') as service_end_date,
  nullif(str_to_date(trim(Date_of_Service_From),'%Y%m%d'),'') as date_of_service,
  nullif(trim(Quantity),'') as service_units,
  cast(ifnull(concat(substring(trim(Charge_Amount),1, length(trim(Charge_Amount)) - 2),'.',right(trim(Charge_Amount),2)),0) as decimal(14,2)) as amt_billed,
  cast(ifnull(concat(substring(trim(paid_amount),1, length(trim(paid_amount)) - 2),'.',right(trim(paid_amount),2)),0) as decimal(14,2)) +
    cast(ifnull(concat(substring(trim(Prepaid_Amount),1, length(trim(Prepaid_Amount)) - 2),'.',right(trim(Prepaid_Amount),2)),0) as decimal(14,2)) as amt_paid,
  cast(ifnull(concat(substring(trim(Deductible_Amount),1, length(trim(Deductible_Amount)) - 2),'.',right(trim(Deductible_Amount),2)),0) as decimal(14,2)) as amt_deductible, 
  cast(ifnull(concat(substring(trim(Copay_Amount),1, length(trim(Copay_Amount)) - 2),'.',right(trim(Copay_Amount),2)),0) as decimal(14,2)) as amt_copay,
  cast(ifnull(concat(substring(trim(Coinsurance_Amount),1, length(trim(Coinsurance_Amount)) - 2),'.',right(trim(Coinsurance_Amount),2)),0) as decimal(14,2)) as amt_coinsurance,
  nullif(trim(DRG),'') as DRG,
  nullif(case when trim(National_Provider_ID_Billing) = '1111111111' then null
         when trim(National_Provider_ID_Billing) like '123456789%' then null
         when trim(National_Provider_ID_Billing) = '9999999999' then null 
         when length(trim(National_Provider_ID_Billing)) != 10 then null
         else trim(National_Provider_ID_Billing) end,'') as billing_provider_NPI,
  nullif(trim(Other_ICD_CM_Procedure_Code_1),'') as procedure_code_3,
  nullif(trim(Other_ICD_CM_Procedure_Code_2),'') as procedure_code_4,
  nullif(trim(Other_ICD_CM_Procedure_Code_3),'') as procedure_code_5,
  nullif(trim(Other_ICD_CM_Procedure_Code_4),'') as procedure_code_6,
  nullif(trim(Other_ICD_CM_Procedure_Code_5),'') as procedure_code_7,
  nullif(trim(Other_ICD_CM_Procedure_Code_6),'') as procedure_code_8,
  nullif(str_to_date(trim(Paid_Date),'%Y%m%d'),'') as date_paid,
  cast(ifnull(concat(substring(trim(Coordination_of_Benefits_TPL_Liability_Amount),1, length(trim(Coordination_of_Benefits_TPL_Liability_Amount)) - 2),'.',right(trim(Coordination_of_Benefits_TPL_Liability_Amount),2)),0) as decimal(14,2)) +
    cast(ifnull(concat(substring(trim(Other_Insurance_Paid_Amount),1, length(trim(Other_Insurance_Paid_Amount)) - 2),'.',right(trim(Other_Insurance_Paid_Amount),2)),0) as decimal(14,2))  as amt_cob,
  cast(ifnull(concat(substring(trim(Allowed_amount),1, length(trim(Allowed_amount)) - 2),'.',right(trim(Allowed_amount),2)),0) as decimal(14,2)) as amt_allowed,
  nullif(trim(ICD_Indicator),'') as icd_version,
  cast(ifnull(concat(substring(trim(Medicare_Paid_Amount),1, length(trim(Medicare_Paid_Amount)) - 2),'.',right(trim(Medicare_Paid_Amount),2)),0) as decimal(14,2))  as misc_2,
  'Medicare_Paid_Amount' as misc_2_desc,
  cast(ifnull(concat(substring(trim(Patient_Total_Out_of_Pocket_Amount),1, length(trim(Patient_Total_Out_of_Pocket_Amount)) - 2),'.',right(trim(Patient_Total_Out_of_Pocket_Amount),2)),0) as decimal(14,2)) as amt_patient_responsibility,
  nullif(case when trim(InNetwork_Indicator) = '1' then 'Y'
              when trim(InNetwork_Indicator) != '1' then 'N'
              when trim(InNetwork_Indicator) is null then null end,'') as network,
  nullif(trim(Admission_Date),'') as misc_4,
  'Admission date' as misc_4_desc,
  '20150101_20161231_APCD_Medical.txt || 20170101_20180831_APCD_Medical.txt' as raw_file_name
from nhp.medical_raw;

/* rx */
ALTER TABLE `nhp`.`rx_raw` 
ADD INDEX `NDC` (`Drug_Code`(10) ASC);

INSERT INTO nhp.rx_claims
(uh_payer_id,lob,claim_number,member_uniq_id,pharmacy_id,pharmacy_name,pharmacy_city,pharmacy_state,pharmacy_zip,claim_status,NDC,
ndc_drug_name,ndc_generic,ndc_maintenance,date_filled,metric_units,days_supply,amt_billed,amt_paid,ingredient_cost,dispensing_fee,amt_copay,amt_coinsurance,amt_deductible,prescribing_provider_name,
prescribing_provider_npi,retail_mail,date_paid,cob_amt,amt_allowed,GDI_number,misc_1, misc_1_desc, misc_2, misc_2_desc,misc_3,misc_3_desc,raw_file_name)
select
nullif(trim(National_Plan_ID),'') as uh_payer_id,
  case when nullif(trim(Insurance_Type_Code_Product),'') = '12' then 'Commercial-PPO'
       when nullif(trim(Insurance_Type_Code_Product),'') = '30' then 'Commercial-ACO'
       when nullif(trim(Insurance_Type_Code_Product),'') = 'HM' then 'Commercial'
       when nullif(trim(Insurance_Type_Code_Product),'') = 'MC' then 'Medicaid'
       when nullif(trim(Insurance_Type_Code_Product),'') = 'CC' then 'Medicaid-CCHIP' end as lob,
nullif(trim(Payer_Claim_Control_Number),'') as claim_number,
nullif(trim(Carrier_Specific_Unique_Member_ID),'') as member_uniq_id,
nullif(trim(Pharmacy_Number),'') as pharmacy_id,
nullif(trim(Pharmacy_Name),'') as pharmacy_name,
nullif(trim(Pharmacy_Location_City),'') as pharmacy_city,
nullif(trim(Pharmacy_Location_State),'') as pharmacy_state,
nullif(trim(Pharmacy_ZIP_Code),'') as pharmacy_zip,
  nullif(case when trim(Claim_Status) = '4' then 'D'
	     when trim(Claim_Status) = '22' then 'V'
         when trim(Claim_Status) is null then null
         else 'P' end,'') as claim_status, 
nullif(trim(Drug_Code),'') as NDC,
nullif(trim(ndc.TC_GPI_Name),'') as ndc_drug_name,
nullif(trim(ndc.generic_ind),'') as ndc_generic,
nullif(trim(ndc.maintenance_ind),'') as ndc_maintenance,
nullif(str_to_date(trim(Date_Prescription_Filled),'%Y%m%d'),'') as date_filled,
nullif(trim(Quantity_Dispensed),'') as metric_units,
nullif(trim(Days_Supply),'') as days_supply,
cast(ifnull(concat(substring(trim(Charge_Amount),1, length(trim(Charge_Amount)) - 2),'.',right(trim(Charge_Amount),2)),0) as decimal(14,2)) as amt_billed,
cast(ifnull(concat(substring(trim(Paid_Amount),1, length(trim(Paid_Amount)) - 2),'.',right(trim(Paid_Amount),2)),0) as decimal(14,2)) as amt_paid,
cast(ifnull(concat(substring(trim(Ingredient_Cost_List_Price),1, length(trim(Ingredient_Cost_List_Price)) - 2),'.',right(trim(Ingredient_Cost_List_Price),2)),0) as decimal(14,2)) as ingredient_cost,
cast(ifnull(concat(substring(trim(Dispensing_Fee),1, length(trim(Dispensing_Fee)) - 2),'.',right(trim(Dispensing_Fee),2)),0) as decimal(14,2)) as dispensing_fee,
cast(ifnull(concat(substring(trim(Copay_Amount),1, length(trim(Copay_Amount)) - 2),'.',right(trim(Copay_Amount),2)),0) as decimal(14,2)) as amt_copay,
cast(ifnull(concat(substring(trim(Coinsurance_Amount),1, length(trim(Coinsurance_Amount)) - 2),'.',right(trim(Coinsurance_Amount),2)),0) as decimal(14,2)) as amt_coinsurance,
cast(ifnull(concat(substring(trim(Deductible_Amount),1, length(trim(Deductible_Amount)) - 2),'.',right(trim(Deductible_Amount),2)),0) as decimal(14,2)) as amt_deductible,
nullif(trim(concat(ifnull(trim(Prescribing_Physician_First_Name),''),' ',ifnull(trim(Prescribing_Physician_Middle_Name),''),' ',ifnull(trim(Prescribing_Physician_Last_Name),''))),'') as prescribing_provider_name,
nullif(trim(National_Provider_ID_Prescribing),'') as prescribing_provider_npi,
nullif(trim(Mail_Order_pharmacy),'') as retail_mail,
nullif(str_to_date(trim(Paid_Date),'%Y%m%d'),'') as date_paid,
 cast(ifnull(concat(substring(trim(Coordination_of_Benefits_TPL_Liability_Amount),1, length(trim(Coordination_of_Benefits_TPL_Liability_Amount)) - 2),'.',right(trim(Coordination_of_Benefits_TPL_Liability_Amount),2)),0) as decimal(14,2)) +
    cast(ifnull(concat(substring(trim(Other_Insurance_Paid_Amount),1, length(trim(Other_Insurance_Paid_Amount)) - 2),'.',right(trim(Other_Insurance_Paid_Amount),2)),0) as decimal(14,2))  as cob_amt,
cast(ifnull(concat(substring(trim(Allowed_amount),1, length(trim(Allowed_amount)) - 2),'.',right(trim(Allowed_amount),2)),0) as decimal(14,2)) as amt_allowed,
nullif(trim(ndc.Generic_product_identifier),'') as GDI_number,
nullif(trim(claims.Drug_Name),'') as misc_1,
'APCD Drug Name' as misc_1_desc, 
nullif(trim(Generic_Drug_Indicator),'') as misc_2,
'APCD Generic_Drug_Indicator' as misc_2_desc,
  nullif(case when trim(National_Provider_ID_Pharmacy) = '1111111111' then null
         when trim(National_Provider_ID_Pharmacy) like '123456789%' then null
         when trim(National_Provider_ID_Pharmacy) = '9999999999' then null 
         when length(trim(National_Provider_ID_Pharmacy)) != 10 then null
         else trim(National_Provider_ID_Pharmacy) end,'') as misc_3,
'Pharmacy NPI' as misc_3_desc,
'20150101_20180831_APCD_Pharmacy.txt' as raw_file_name
from rx_raw claims
  left join medispan.NDC_details ndc
    on claims.Drug_Code = ndc.NDC;
    
