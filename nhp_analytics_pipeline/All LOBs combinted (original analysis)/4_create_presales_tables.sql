/* Create tables to store output of intial profiling work */

drop table if exists medical_overview;
drop table if exists medical_financial_check;
drop table if exists pos_overview;
drop table if exists icd_counts_by_category;
drop table if exists rx_overview;
drop table if exists rx_financial_check;
drop table if exists rx_drug_type_overview;
drop table if exists eligibility_overview;
drop table if exists active_row_check;
drop table if exists member_reconcilation_check;
drop table if exists per_member_spend;

create table medical_overview
(
rowCount int,
claimsCount int,
paidClaimsCount int,
memberCount int,
totalSpend decimal (14,2),
totalExposure  decimal (14,2),
posCount int,
icd_1Count int,
icd_2Count int,
icd_3Count int,
icd_4Count int,
icd_5Count int,
icd_6Count int,
icd_7Count int,
icd_8Count int,
icd_9Count int,
icd_10Count int,
procedureCount int,
revCodeCount int,
DRGCount int,
EariestDateOfService date,
LatestDateOfService date
);

create table medical_financial_check
(
badClaimsCount int,
badRowCount int,
badMemberCount int,
badTotalSpend decimal(14,2),
badTotalExposure decimal(14,2),
allClaimsCount int,
allRowCount int,
allMemberCount int,
allTotalSpend decimal(14,2),
allTotalExposure decimal(14,2),
badClaimsPercent decimal(14,4),
badMemberPercent decimal(14,4),
badRowPercent decimal(14,4),
badSpendPercent decimal(14,4),
badExposurePercent decimal(14,4)
);

create table pos_overview
(
claimsCount int,
memberCount int,
cmsID varchar(2),
pos_name varchar(255)
);

create table icd_counts_by_category
(
claimsCount int,
memberCount int,
category varchar(255)
);

create table rx_overview
(
rowCount int,
claimsCount int,
paidClaimsCount int,
memberCount int,
totalSpend decimal (14,2),
totalExposure  decimal (14,2),
totalNDCCount int,
nonMatchedNDCClaimsCount int,
nonMatchedNDCCodesCount int,
pharmacyNPICount int,
prescribingProviderCount int,
EariestDateFilled date,
LatestDateFilled date
);

create table rx_financial_check
(
badClaimsCount int,
badRowCount int,
badMemberCount int,
badTotalSpend decimal(14,2),
badTotalExposure decimal(14,2),
allClaimsCount int,
allRowCount int,
allMemberCount int,
allTotalSpend decimal(14,2),
allTotalExposure decimal(14,2),
badClaimsPercent decimal(14,4),
badMemberPercent decimal(14,4),
badRowPercent decimal(14,4),
badSpendPercent decimal(14,4),
badExposurePercent decimal(14,4)
);

create table rx_drug_type_overview
(
brandOrGeneric varchar(1),
maintainenceIndicator varchar(1),
Counts int
);

create table eligibility_overview
(
totalRows int,
totalActiveRows int,
memberCount int,
susbcriberCount int,
groupCount int,
relationshipCount int,
earliestEligigbilityRecord date,
latestEligibilityRecord date,
earliestActiveRecord date,
latestActiveRecord date
);

create table active_row_check
(
memberID varchar(255),
rowCount int
);

create table member_reconcilation_check
(
memberCount int,
description varchar(255)
);

create table per_member_spend
(
medicalPerMemberSpend decimal(14,2),
rxPerMemberSpend decimal(14,2),
totalPerMemberSpend decimal(14,2)
);
