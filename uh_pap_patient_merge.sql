
Patient Merge Steps
------------------------------------------------------------------------------------------

1 - find the patient records to compare
SELECT * FROM utilizehealth.patients where PatientId IN (XX1, XX2);

2 - determine if merge is forward or backward and save a copy of original record


3 - create update statement with WHERE CLAUSE!!!!!!!!!!!!!!!!!!!!
update utilizehealth.patients
	set Phone_old = '...', 
		Address = '...',
        InsuranceNumber = '...',
        SSN = '...',
        StateId = ...,
        IsEligible = ...,
        WaveId = ...,
        member_di_key = '...'
	Where PatientId = XX1;